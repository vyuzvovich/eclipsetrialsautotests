package testcase;

import base.BaseTest;
import businessobjects.enums.Users;
import businessutils.UserCreationUtil;
import org.testng.annotations.Test;


public class CreateUsers extends BaseTest {

    @Test
    public void createUserAndSetGroupForPrimaryAdmin() {
        Users user = Users.PRIMARY_ADMIN;
        user.assignDefaultGroupManagementMap();

        //NOTE: assert is within UserCreatinUtil
        UserCreationUtil.createUserAndAssignGroup(user);
    }

    @Test
    public void createUserAndSetGroupForAnalystAdmin() {
        Users user = Users.ANALYST_ADMIN;
        user.assignDefaultGroupManagementMap();

        //NOTE: assert is within UserCreatinUtil
        UserCreationUtil.createUserAndAssignGroup(user);
    }

    @Test
    public void createUserAndSetGroupForDatasourceAdmin() {
        Users user = Users.DATASOURCE_ADMIN;
        user.assignDefaultGroupManagementMap();

        //NOTE: assert is within UserCreatinUtil
        UserCreationUtil.createUserAndAssignGroup(user);
    }

    @Test
    public void createUserAndSetGroupForAnalystMember() {
        Users user = Users.ANALYST_MEMBER;
        user.assignDefaultGroupManagementMap();

        //NOTE: assert is within UserCreatinUtil
        UserCreationUtil.createUserAndAssignGroup(user);
    }

    @Test
    public void createUserAndSetGroupForDatasourceMember() {
        Users user = Users.DATASOURCE_MEMBER;
        user.assignDefaultGroupManagementMap();

        //NOTE: assert is within UserCreatinUtil
        UserCreationUtil.createUserAndAssignGroup(user);
    }

    @Test
    public void createUserAndSetGroupForAnalystMemberDatasourceMember() {
        Users user = Users.ANALYST_MEMBER_DATASOURCE_MEMBER;
        user.assignDefaultGroupManagementMap();

        //NOTE: assert is within UserCreatinUtil
        UserCreationUtil.createUserAndAssignGroup(user);
    }

    @Test
    public void createUserAndSetGroupForAdminMember() {
        Users user = Users.ADMIN_MEMBER;
        user.assignDefaultGroupManagementMap();

        //NOTE: assert is within UserCreatinUtil
        UserCreationUtil.createUserAndAssignGroup(user);
    }
}
