package testcase;

import base.BaseTest;
import businessobjects.Project;
import businessobjects.enums.Users;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.HomePage;
import pages.LocatePage;
import pages.LoginPage;
import pages.Page;

public class CreateAndDeleteProject extends BaseTest {

    private Project project = Project.randomProject();
    private Users user = Users.PRIMARY_ADMIN;

    @BeforeClass(description = "Login with existing user", alwaysRun = true)
    public void login() {
        LoginPage page = new LoginPage().openPage();
        page.loginWithUser(user);
    }

    @Test
    public void verifyUserIsLoggedIn() {
        SoftAssert softAssert = new SoftAssert();
        HomePage homePage = new HomePage();
        softAssert.assertTrue(homePage.checkPageIsLoaded(), "User is not located on 'Home' page");
        softAssert.assertTrue(homePage.getMenu().verifyUserSubmenuTitle(user.getName()),
                "Top menu title doesn't match username");
        softAssert.assertAll();
    }

    @Test()
    public void verifyNewProjectCreated() {
        HomePage homePage = new HomePage();
        LocatePage locatePage = homePage.createNewProject(project.getName());
        homePage = locatePage.getMenu().navigateToPage(Page.HOME_PAGE);
        Assert.assertTrue(homePage.verifyProjectExists(project.getName()), "Project is not created");
    }

    @Test(dependsOnMethods = "verifyNewProjectCreated")
    public void deleteProject() {
        HomePage homePage = new HomePage();
        homePage.deleteProject(project.getName());
        Assert.assertFalse(homePage.verifyProjectExists(project.getName()));
    }

}
