package testcase;

import base.BaseTest;
import businessobjects.DataSource;
import businessobjects.Project;
import businessobjects.csvmapping.CSVFile;
import businessobjects.enums.Users;
import businessutils.AuthenticationUtil;
import businessutils.HomeProjectManagementUtil;
import businessutils.PreconditionUtil;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ClassificationPage;
import pages.HomePage;
import waiters.Waiters;

public class CloneProject extends BaseTest {
    private Project parentProject = Project.randomProject("_cloneTest");
    private Users user = Users.FIRST_ADMIN;

    @Test()
    public void createProjectForCloning() {
        DataSource dataSource = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV).addCSV(CSVFile.DIAGNOSIS)
                .addCSV(CSVFile.DEMOGRAPHICS).addDataModel(CSVFile.DEMOGRAPHICS, CSVFile.DIAGNOSIS, "PID", "PID").build();
        AuthenticationUtil.loginWithSpecifiedUser(user);
        PreconditionUtil.createProjectUpToRiskManagement(parentProject, dataSource);
        Assert.assertTrue(new HomePage().isRiskMeasurementSuccessfull(parentProject.getName()), "Risk management is failed for "+ parentProject.getName());
    }

    @Test(dependsOnMethods = "createProjectForCloning")
    public void cloneProject() {
        AuthenticationUtil.loginWithSpecifiedUser(user);
        HomePage homePage = new HomePage();
        Project clonedProject = parentProject.copy();
        homePage.cloneProject(parentProject.getName(), clonedProject.getName());
        Waiters.waitForAnimationCompleteOnPage(30);
        Assert.assertTrue(homePage.verifyProjectExists(clonedProject.getName()), "Project  '" + clonedProject.getName() + "' is not cloned");
        HomeProjectManagementUtil.clickOnProject(clonedProject);
        new ClassificationPage().navigateToDataGroups().navigateToContextPage().measureRisk();
        homePage.waitForRiskMeasurementCompleted(clonedProject.getName());
        Assert.assertTrue(homePage.isRiskMeasurementSuccessfull(clonedProject.getName()), "Risk management is failed for "+ clonedProject.getName());
    }
}
