package testcase;

import base.BaseTestForRegisteredUser;
import businessobjects.DataSource;
import businessobjects.Project;
import businessobjects.csvmapping.CSVFile;
import businessobjects.dategeneralization.DateGeneralization;
import businessobjects.dategeneralization.Term;
import businessobjects.enums.DataTypes;
import businessobjects.enums.Masking;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.AddToClassificationPage;
import pages.ClassificationPage;
import pages.DirectIdentifiersPage;
import pages.HomePage;
import pages.QuasiIdentifiersPage;
import pages.RiskOverviewPage;
import pages.UploadCSVPage;

public class FrameworkUsingTemplate extends BaseTestForRegisteredUser {

    @Test
    public void loadAndVerifyDataSource(){
        SoftAssert softAssert = new SoftAssert();
        HomePage homePage= new HomePage();
        DataSource dataSource = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV).addCSV(CSVFile.DIAGNOSIS)
                .addCSV(CSVFile.DEMOGRAPHICS).addDataModel(CSVFile.DEMOGRAPHICS, CSVFile.DIAGNOSIS, "PID", "PID").build();
        Project project = Project.randomProject();
        UploadCSVPage upload = homePage.createNewProject(project.getName()).createDataSource(dataSource);
        upload.upload(dataSource);
        softAssert.assertTrue(upload.verifyDataSourceUploaded(dataSource), "Data Source is not loaded correctly");
        AddToClassificationPage addToClassificationPage = upload.navigateToAddClassification();
        softAssert.assertTrue(addToClassificationPage.verifyTablesAvailable(dataSource));
        softAssert.assertTrue(addToClassificationPage.verifyColumns(dataSource));
        ClassificationPage classificationPage = addToClassificationPage.navigateToDataModel().setTableRelationShips(dataSource)
                .navigateToClassificationPage();
        softAssert.assertTrue(classificationPage.verifyClassificationLoaded(dataSource));
        homePage = classificationPage.addClassification("Gender", "Gender", DataTypes.STRING).addDefaultClassifications().
                navigateToDataGroups().navigateToContextPage().measureRisk().waitForRiskMeasurementCompleted(project.getName());
        RiskOverviewPage riskOverviewPage = homePage.navigateToRiskOverviewPageForRMProject(project.getName());
        softAssert.assertFalse(riskOverviewPage.isResultCompliant());

        DirectIdentifiersPage directIdentifiersPage = riskOverviewPage.navigateToRiskDetailsPage().navigateToDirectIdentifiers();
        directIdentifiersPage.addMasking(dataSource, "visitID", Masking.DIRECT_IDENTIFIERS.MASK_AS_ID).addDefaultMasking();
        QuasiIdentifiersPage quasiIdentifiersPage = directIdentifiersPage.navigateToQuasiIdentifiers().
                addMasking(dataSource, "DateofDiagnosis", new Masking.GeneralizationDate().setGeneralization(new DateGeneralization(new Term(2000, 1, Term.IntervalType.WEEKS))))
                .addDefaultMasking();
        homePage = quasiIdentifiersPage.navigateToDestinationPage().deIdentifyWithDefaultSettings().waitForDeidentificationCompleted(project.getName());
        CSVFile[] files = homePage.downloadCSVResults(project.getName());
        dataSource.addDeidentificationResults(files);
        softAssert.assertTrue(dataSource.verifyMaskingApplied() && dataSource.verifyShuffling());
        softAssert.assertAll();
    }
}
