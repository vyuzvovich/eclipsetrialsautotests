package testcase;

import base.BaseTest;
import businessobjects.ClassificationTable;
import businessobjects.DataSource;
import businessobjects.Project;
import businessobjects.csvmapping.CSVFile;
import businessobjects.enums.Users;
import businessutils.AuthenticationUtil;
import businessutils.DeIdentifyUtils;
import businessutils.HomeProjectManagementUtil;
import businessutils.LocateDataSourcesUtil;
import businessutils.ModelClassificationUtil;
import businessutils.PreconditionUtil;
import core.driver.DriverManager;
import core.logger.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.ClassificationPage;
import pages.ContextPage;
import pages.DeIdentityPage;
import pages.DirectIdentifiersPage;
import pages.HomePage;
import pages.RiskOverviewPage;
import utils.PdfFileReader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplyFullFlowTemplateTwoProjects extends BaseTest {

    private Project template = Project.randomProject("_Template");
    private Users user = Users.FIRST_ADMIN;
    private DataSource dataSource = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV).addCSV(CSVFile.DIAGNOSIS)
            .addCSV(CSVFile.DEMOGRAPHICS).addDataModel(CSVFile.DEMOGRAPHICS, CSVFile.DIAGNOSIS, "PID", "PID").build();
    private Map<ClassificationTable, ClassificationTable> matchingMap = new HashMap<>();
    private String compliance="100.00%";

    @Test()
    public void createProjectAndSetAsTemplate() {
        SoftAssert softAssert = new SoftAssert();
        AuthenticationUtil.loginWithSpecifiedUser(user);

        PreconditionUtil.createProjectUpToRiskManagement(template, dataSource);

        String templateName = template.getName();
        softAssert.assertTrue(HomeProjectManagementUtil.isRiskMeasurementSuccessfull(templateName),
                "Risk management is failed for " + templateName);
        //


        RiskOverviewPage riskOverviewPage = new HomePage().navigateToRiskOverviewPageForRMProject(template.getName());
        softAssert.assertFalse(riskOverviewPage.isResultCompliant(), "Result is compliant, but should be not!");
        softAssert.assertTrue(riskOverviewPage.getPercentageCompliant().contains(compliance));
        //
        DirectIdentifiersPage directIdentifiersPage = riskOverviewPage.navigateToRiskDetailsPage().navigateToDirectIdentifiers();
        DeIdentifyUtils.loadDefaultsForDirectIdentifiersPage();
        DeIdentifyUtils.loadDefaultsForQuasiIdentifiersPage();

        DeIdentifyUtils.runDeIdentification(templateName, DeIdentityPage.ALWAYS);
        //
        softAssert.assertTrue(HomeProjectManagementUtil.isDeIdentificationSuccessfull(templateName),
                "De-Identification is failed for " + templateName);;

        HomeProjectManagementUtil.setProjectAsTemplate(templateName);
        softAssert.assertTrue(HomeProjectManagementUtil.isProjectMarkedAsTemplate(templateName),
                "Project '"+templateName+"' is not marked as template");


        riskOverviewPage = HomeProjectManagementUtil.navigateToRiskOverviewPageForDeIdentifiedProject(templateName);
        softAssert.assertTrue(riskOverviewPage.isResultCompliant(), "Result is not compliant!");

        String reportSummaryName = riskOverviewPage.downloadReportSummary(templateName);
        PdfFileReader pdfFileReader =  new PdfFileReader(DriverManager.MOUNT_DIR + reportSummaryName);

        String str = pdfFileReader.getTextFromPdfPage(1);
        Logger.info("PDF first page result is: " + str);

        ////
        softAssert.assertAll();
    }

    @Test(dependsOnMethods = "createProjectAndSetAsTemplate")
    public void createNewProjectAndApplyTemplate() {
        SoftAssert softAssert = new SoftAssert();
        AuthenticationUtil.loginWithSpecifiedUser(user);

        DataSource dataSourceRecip = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV).addCSV(CSVFile.DIAGNOSIS)
                .addCSV(CSVFile.DEMOGRAPHICS).addDataModel(CSVFile.DEMOGRAPHICS, CSVFile.DIAGNOSIS, "PID", "PID").build();


        Project projectWTemplate = Project.randomProject("_wTemplate");
        HomeProjectManagementUtil.createNewProjectOnHomePage(projectWTemplate);

        HomeProjectManagementUtil.clickOnProject(projectWTemplate);

        LocateDataSourcesUtil.createNewCSVDataSource(dataSourceRecip);

        LocateDataSourcesUtil.addAllColumnsToClassification(dataSourceRecip);

        //Create matchingMap for matching project table with template table, match Masking_L1 with Masking_LU
        List<ClassificationTable> classificationTableListRecip = dataSourceRecip.getClassificationTables();
        List<ClassificationTable> classificationTableListTemp = dataSource.getClassificationTables();
        classificationTableListRecip.forEach(c -> {
            //TODO: ask, what way to use for matching map creation by test logics? Then move to businessUtils
            //FOR now: just as check by first 7 symbols in names matching
            ClassificationTable ct = classificationTableListTemp.stream().filter(t -> c.getName().substring(0, 12).equals(t.getName().substring(0, 12))).findFirst().orElse(null);
            matchingMap.put(c, ct);
        });

        LocateDataSourcesUtil.applyTemplateAndMatchTables(template.getName(), matchingMap); //TODO: remove after

        ModelClassificationUtil.fillClassificationTableWithDefaults();
        ContextPage contextPage = new ClassificationPage().navigateToDataGroups().navigateToContextPage();
        contextPage.measureRisk();
        String name = projectWTemplate.getName();
        new HomePage().waitForRiskMeasurementCompleted(projectWTemplate.getName());

        softAssert.assertTrue(HomeProjectManagementUtil.isRiskMeasurementSuccessfull(name),
                "Risk management is failed for " + name);
        //


        RiskOverviewPage riskOverviewPage = new HomePage().navigateToRiskOverviewPageForRMProject(name);
        softAssert.assertFalse(riskOverviewPage.isResultCompliant(), "Result is compliant, but should be not!");
        softAssert.assertTrue(riskOverviewPage.getPercentageCompliant().contains(compliance));
        //
        DirectIdentifiersPage directIdentifiersPage = riskOverviewPage.navigateToRiskDetailsPage().navigateToDirectIdentifiers();
        //
        DeIdentifyUtils.loadDefaultsForDirectIdentifiersPage();
        DeIdentifyUtils.loadDefaultsForQuasiIdentifiersPage();

        DeIdentifyUtils.runDeIdentification(name, DeIdentityPage.ALWAYS);
        //
        softAssert.assertTrue(HomeProjectManagementUtil.isDeIdentificationSuccessfull(name),
                "De-Identification is failed for " + name);

        softAssert.assertAll();

    }

  /*  @Test
    public void pdfTest(){
        AuthenticationUtil.loginWithSpecifiedUser(user);
        String templateName =  "Project 2019-04-29 11-39-03_Template";
        RiskOverviewPage riskOverviewPage = HomeProjectManagementUtil.navigateToRiskOverviewPage(templateName);
        String reportSummaryName = riskOverviewPage.downloadReportSummary(templateName);
        PdfFileReader pdfFileReader =  new PdfFileReader(DriverManager.MOUNT_DIR + reportSummaryName);

        String str = pdfFileReader.getTextFromPdfPage(1);
        Logger.info("PDF first page result is: ");
    }
 */
}
