package testcase;

import base.BaseTest;
import businessobjects.DataSource;
import businessobjects.Project;
import businessobjects.csvmapping.CSVFile;
import businessobjects.enums.Users;
import businessutils.AuthenticationUtil;
import businessutils.PreconditionUtil;
import businessutils.RiskMeasurementUtil;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.HomePage;


public class ValidateRiskForMedDRAHLTSingleNumeric extends BaseTest {

    private Users user = Users.FIRST_ADMIN;


    @Test
    public void validateRiskManagementMeansForMedDRAHLT() {

        double expectedStrict = 0.08000000000000002;
        double expectedPublic = 0.08000000000000003;
        double expectedAcquaintance = 0.5265276712603344;
        double expectedTraditional = 0.5298187561464012;
        int population = 25;

        Project project = Project.randomProject("_MedDRA_01");
        DataSource dataSource = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV)
                .addCSV(CSVFile.MEDDRA01L1).addCSV(CSVFile.MEDDRA01L2)
                .addDataModel(CSVFile.MEDDRA01L2, CSVFile.MEDDRA01L1, "PID", "PID").build();

        AuthenticationUtil.loginWithSpecifiedUser(user);

        PreconditionUtil.createProjectUpToRiskManagementWithCustomRMSettings(project, dataSource, population);

        String name = project.getName();
        Assert.assertTrue(new HomePage().isRiskMeasurementSuccessfull(name), "Risk management is failed for " + name);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(RiskMeasurementUtil.getStrictRisk(name), expectedStrict);
        softAssert.assertEquals(RiskMeasurementUtil.getPublicRisk(name), expectedPublic);
        softAssert.assertEquals(RiskMeasurementUtil.getAcquaintanceRisk(name), expectedAcquaintance);
        softAssert.assertEquals(RiskMeasurementUtil.getTraditionalRisk(name), expectedTraditional);
        softAssert.assertAll();
    }

}
