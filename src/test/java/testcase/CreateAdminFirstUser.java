package testcase;

import base.BaseTest;
import businessobjects.enums.Users;
import core.logger.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AdminPanelPage;
import pages.GroupManagementPage;
import pages.HomePage;
import pages.LoginPage;
import pages.Page;

public class CreateAdminFirstUser extends BaseTest {

    private Users user = Users.FIRST_ADMIN;

    @Test()
    public void createFirstAdminAccount() {
        LoginPage loginPage = new LoginPage().openPage();
        if (!loginPage.isFirstUserCreationScreen()) {
            Logger.info("Not a first user creation page! First user is already created");

        } else {
            loginPage.createAccount(user);
            Assert.assertTrue(loginPage.verifyAccountIsCreated(user), "User is not located on 'Home' page");
        }
    }

    @Test(priority = 1, dependsOnMethods = "createFirstAdminAccount")
    public void addFirstAdminUserToGroups() {
        user.assignDefaultGroupManagementMap();

        LoginPage page = new LoginPage().openPage();
        page.loginWithUser(user);
        AdminPanelPage panelPage = new HomePage().getUserSubMenu().navigateToPage(Page.ADMIN_PANEL_PAGE);
        GroupManagementPage groupManagementPage = panelPage.getAdminPageTabs().navigateToPage(Page.GROUP_MANAGEMENT_PAGE);

        Assert.assertTrue(groupManagementPage.addUserToGroups(Users.FIRST_ADMIN));
    }
}
