package testcase;

import base.BaseTest;
import businessobjects.ClassificationTable;
import businessobjects.DataSource;
import businessobjects.Project;
import businessobjects.csvmapping.CSVFile;
import businessobjects.enums.Users;
import businessutils.AuthenticationUtil;
import businessutils.HomeProjectManagementUtil;
import businessutils.LocateDataSourcesUtil;
import businessutils.PreconditionUtil;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplyTemplateToProject extends BaseTest {

    private Project template = Project.randomProject("_template");
    private Users user = Users.PRIMARY_ADMIN;
    private DataSource dataSource = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV).addCSV(CSVFile.DIAGNOSIS)
            .addCSV(CSVFile.DEMOGRAPHICS).addDataModel(CSVFile.DEMOGRAPHICS, CSVFile.DIAGNOSIS, "PID", "PID").build();
    private Map<ClassificationTable, ClassificationTable> matchingMap = new HashMap<>();

    @Test()
    public void createProjectAndSetAsTemplate() {

        AuthenticationUtil.loginWithSpecifiedUser(user);

        PreconditionUtil.createProjectUpToRiskManagement(template, dataSource);

        String templateName = template.getName();
        Assert.assertTrue(new HomePage().isRiskMeasurementSuccessfull(templateName), "Risk management is failed for " + templateName);
        HomeProjectManagementUtil.setProjectAsTemplate(templateName);
        Assert.assertTrue(HomeProjectManagementUtil.isProjectMarkedAsTemplate(templateName), "Project '"+templateName+"' is not marked as template");
    }

    @Test(dependsOnMethods = "createProjectAndSetAsTemplate")
    public void createNewProjectAndApplyTemplate() {
        AuthenticationUtil.loginWithSpecifiedUser(user);

        DataSource dataSourceRecip = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV).addCSV(CSVFile.DIAGNOSIS)
                .addCSV(CSVFile.DEMOGRAPHICS).addDataModel(CSVFile.DEMOGRAPHICS, CSVFile.DIAGNOSIS, "PID", "PID").build();

        Project projectWTemplate = Project.randomProject("_wTemplate");
        HomeProjectManagementUtil.createNewProjectOnHomePage(projectWTemplate);

        HomeProjectManagementUtil.clickOnProject(projectWTemplate);

        LocateDataSourcesUtil.createNewCSVDataSource(dataSourceRecip);

        LocateDataSourcesUtil.addAllColumnsToClassification(dataSourceRecip);

        //Create matchingMap for matching project tables with template tables, match tables with same names
        List<ClassificationTable> classificationTableListRecip = dataSourceRecip.getClassificationTables();
        List<ClassificationTable> classificationTableListTemp = dataSource.getClassificationTables();
        classificationTableListRecip.forEach(c -> {
            ClassificationTable ct = classificationTableListTemp.stream().filter(t -> c.getName().equals(t.getName())).findFirst().orElse(null);
            matchingMap.put(c, ct);
        });

        LocateDataSourcesUtil.applyTemplateAndMatchTables(template.getName(), matchingMap);


        //TODO: add the next after merging!
        /*
        new ClassificationPage().navigateToDataGroups().navigateToContextPage().measureRisk();
        HomePage homePage = new HomePage();
        homePage.waitForRiskMeasurementCompleted(projectWTemplate.getName());
        Assert.assertTrue(homePage.isRiskMeasurementSuccessfull(projectWTemplate.getName()), "Risk management is failed for "+ projectWTemplate.getName());
    */
    }
}
