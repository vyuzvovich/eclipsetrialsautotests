package testcase;

import base.BaseTest;
import businessobjects.enums.Users;
import businessutils.AuthenticationUtil;
import businessutils.PIIAdministrationUtil;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FilterPiiType extends BaseTest {

    private Users user = Users.PRIMARY_ADMIN;

    private String abbrevKey = "SAE";
    private int abbrevKeyExpectedResult = 1;

    private String fullWord = "event";
    //TODO: update after fix of ECTN-166
    private int fullWordExpectedResult = 0;

    private String partWord = "meddr";
    private int partWordExpectedResult = 5;

    @Test
    public void filterPiiTypeAbbreviation() {
        AuthenticationUtil.loginWithSpecifiedUser(user);

        int result = PIIAdministrationUtil.filterPIIType(abbrevKey);
        Assert.assertEquals(result, abbrevKeyExpectedResult);
    }

    @Test
    public void filterPiiFullWord() {
        //TODO: update after fix of ECTN-166
        AuthenticationUtil.loginWithSpecifiedUser(user);

        int result = PIIAdministrationUtil.filterPIIType(fullWord);
        Assert.assertEquals(result, fullWordExpectedResult);
    }

    @Test
    public void filterPiiPartWord() {
        AuthenticationUtil.loginWithSpecifiedUser(user);

        int result = PIIAdministrationUtil.filterPIIType(partWord);
        Assert.assertEquals(result, partWordExpectedResult);
    }


}
