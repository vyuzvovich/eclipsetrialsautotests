package testcase;

import base.BaseTest;
import businessobjects.PIIType;
import businessobjects.enums.DataTypes;
import businessobjects.enums.Users;
import businessutils.AuthenticationUtil;
import businessutils.PIIAdministrationUtil;
import org.testng.annotations.Test;
import utils.DataUtil;

public class CreateAndEditCustomPiiType extends BaseTest {

    private Users user = Users.PRIMARY_ADMIN;
    private DataTypes[] dataTypes_01 = {DataTypes.STRING, DataTypes.LONG, DataTypes.DOUBLE};
    private PIIType customPiiType_01 = new PIIType("Pii_" + DataUtil.underscoreDate(), dataTypes_01);
    private DataTypes[] dataTypes_02 = {DataTypes.STRING, DataTypes.LONG};
    private PIIType customPiiType_02 = new PIIType("Pii_E_" + DataUtil.underscoreDate(),dataTypes_02);

    @Test
    public void createCustomPiiType() {
        AuthenticationUtil.loginWithSpecifiedUser(user);

        //NOTE: asserts are within PIIAdministrationUtil method
        PIIAdministrationUtil.createPiiCustomType(customPiiType_01);
    }

   /* @Test(dependsOnMethods = "createCustomPiiType")
    public void verifyPiiTypeIsAvailableOnClassification() {

    }*/

    @Test(dependsOnMethods = "createCustomPiiType")
    public void editCustomPiiType() {
        AuthenticationUtil.loginWithSpecifiedUser(user);

        //NOTE: asserts are within PIIAdministrationUtil method
        PIIAdministrationUtil.editPiiCustomType(customPiiType_01, customPiiType_02);
    }
}
