package testcase;

import base.BaseTest;
import businessobjects.DataSource;
import businessobjects.Project;
import businessobjects.csvmapping.CSVFile;
import businessobjects.enums.Users;
import businessutils.AuthenticationUtil;
import businessutils.DeIdentifyUtils;
import businessutils.HomeProjectManagementUtil;
import businessutils.PreconditionUtil;
import businessutils.RiskMeasurementUtil;
import core.driver.DriverManager;
import core.logger.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.DeIdentityPage;
import pages.DirectIdentifiersPage;
import pages.HomePage;
import pages.RiskOverviewPage;
import utils.PdfFileReader;


public class ValidateRiskForMedDRAAllCodes extends BaseTest {

    private Users user = Users.FIRST_ADMIN;
    private String compliance="100.00%";

    @Test
    public void validateRiskManagementMeansForMedDRAAllLevels() {
        double expectedStrict = 0.004;
        double expectedPublic = 0.004000000000000001;
        double expectedAcquaintance = 0.7828617492871183;
        double expectedTraditional = 0.8463346437431056;
        int population = 500;

        Project parentProject = Project.randomProject("_MedDRA_08a");
        DataSource dataSource = DataSource.newDataSource().setDataSourceType(DataSource.DataSourceType.CSV)
                .addCSV(CSVFile.MEDDRA08AL1).addCSV(CSVFile.MEDDRA08AL2)
                .addDataModel(CSVFile.MEDDRA08AL2, CSVFile.MEDDRA08AL1, "PID", "PID").build();

        AuthenticationUtil.loginWithSpecifiedUser(user);

        PreconditionUtil.createProjectUpToRiskManagementWithCustomRMSettings(parentProject, dataSource, population);

        Assert.assertTrue(new HomePage().isRiskMeasurementSuccessfull(parentProject.getName()), "Risk management is failed for " + parentProject.getName());

        String name = parentProject.getName();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(RiskMeasurementUtil.getStrictRisk(parentProject.getName()), expectedStrict);
        softAssert.assertEquals(RiskMeasurementUtil.getPublicRisk(parentProject.getName()), expectedPublic);
        softAssert.assertEquals(RiskMeasurementUtil.getAcquaintanceRisk(parentProject.getName()), expectedAcquaintance);
        softAssert.assertEquals(RiskMeasurementUtil.getTraditionalRisk(parentProject.getName()), expectedTraditional);

        HomeProjectManagementUtil.closeLogsModalWindow(name);

        RiskOverviewPage riskOverviewPage = new HomePage().navigateToRiskOverviewPageForRMProject(parentProject.getName());

        riskOverviewPage.closeWarningsIfExist();
        softAssert.assertFalse(riskOverviewPage.isResultCompliant(), "Result is compliant, but should be not!");
        softAssert.assertTrue(riskOverviewPage.getPercentageCompliant().contains(compliance));

        //
        String reportSummaryName = riskOverviewPage.downloadReportSummary(name);
        PdfFileReader pdfFileReader =  new PdfFileReader(DriverManager.MOUNT_DIR + reportSummaryName);
        String str = pdfFileReader.getTextFromPdfPage(1);
        softAssert.assertTrue(str.substring(0, 38).equals(name), "First 38 symbols are "+str.substring(0, 38));
        Logger.info("Parsed from PDF Summary report: " + str);
        //
        DirectIdentifiersPage directIdentifiersPage = riskOverviewPage.navigateToRiskDetailsPage().navigateToDirectIdentifiers();
        DeIdentifyUtils.loadDefaultsForDirectIdentifiersPage();
        DeIdentifyUtils.loadDefaultsForQuasiIdentifiersPage();

        DeIdentifyUtils.runDeIdentification(name, DeIdentityPage.ALWAYS);
        //
        softAssert.assertTrue(HomeProjectManagementUtil.isDeIdentificationSuccessfull(name),
                "De-Identification is failed for " + name);;

        HomeProjectManagementUtil.setProjectAsTemplate(name);
        softAssert.assertTrue(HomeProjectManagementUtil.isProjectMarkedAsTemplate(name),
                "Project '"+name+"' is not marked as template");


        riskOverviewPage = HomeProjectManagementUtil.navigateToRiskOverviewPageForDeIdentifiedProject(name);
        softAssert.assertTrue(riskOverviewPage.isResultCompliant(), "Result is not compliant!");

        reportSummaryName = riskOverviewPage.downloadReportSummary(name);
        pdfFileReader =  new PdfFileReader(DriverManager.MOUNT_DIR + reportSummaryName);
        str = pdfFileReader.getTextFromPdfPage(1);
        softAssert.assertTrue(str.substring(0, 38).equals(name), "First 38 symbols are "+str.substring(0, 38));
        Logger.info("Parsed from PDF Summary report: " + str);
        ////

        softAssert.assertAll();
    }
}
