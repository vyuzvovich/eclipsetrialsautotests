package base;

import core.driver.DriverManager;
import core.logger.ScreenShotUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {

    private DriverManager driverManager;

    @BeforeClass(description = "Create webDriver instance", alwaysRun = true)
    public void setUp(){
        driverManager = new DriverManager();
        driverManager.setUp();
    }

    @AfterClass(description = "Close driver", alwaysRun = true)
    public void tearDown(){
        ScreenShotUtil.takeScreenShot();
        driverManager.tearDown();
    }

}
