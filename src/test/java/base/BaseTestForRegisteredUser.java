package base;

import org.testng.annotations.BeforeClass;
import pages.LoginPage;

public class BaseTestForRegisteredUser extends BaseTest {

    @BeforeClass(description = "Login with existing user", alwaysRun = true)
    public void login() {
        LoginPage page = new LoginPage().openPage();
        page.loginWithDefaultUser();
    }

}
