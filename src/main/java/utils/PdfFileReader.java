package utils;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.canvas.parser.PdfTextExtractor;
import com.itextpdf.kernel.pdf.canvas.parser.listener.SimpleTextExtractionStrategy;
import core.logger.Logger;
import waiters.Waiters;

import java.util.Arrays;

public class PdfFileReader {
    public PdfFileReader(String path) {
        pdfDocument =  Arrays.stream(getPossiblePath(path)).map(this::getPdfByPath).filter(s -> s!=null).findFirst().get();
    }


    public String getTextFromPdfPage(int pageNumber){
        SimpleTextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
        String result = PdfTextExtractor.getTextFromPage(pdfDocument.getPage(pageNumber), strategy);
        return result;
    }

   private PdfDocument pdfDocument;

    private String[] getPossiblePath(String path){
        String lastSeconds = path.split("\\.")[0].substring(path.split("\\.")[0].length()-2);
        String[] result = new String[5];
        int lastSecondsInt = Integer.parseInt(lastSeconds);
        for(int i = 0; i < result.length; i++){
            result[i] = getPathSubstring(path, lastSecondsInt+i);
        }
        return result;
    }

    private PdfDocument getPdfByPath(String path) {
        Logger.info("Get Pdf file by path "+path);
        for (int i = 0; i < 3; i++) {
            try {
                pdfDocument = new PdfDocument(new PdfReader(path));
            } catch (Exception e) {
                Logger.debug("Catch Exception "+ e.getMessage());
                Waiters.threadSleep(1);
            }
            if (pdfDocument !=null)
                break;
        }
        return pdfDocument;
    }

    private String getPathSubstring(String path, int lastSecond){
        return path.substring(0, path.length()-6)+ String.format("%02d", lastSecond) + path.substring(path.length()-4);
    }

}
