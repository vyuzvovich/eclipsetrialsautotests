package utils;
import businessobjects.ClassificationObject;
import businessobjects.PIIType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

public class JSONParser {

        @SuppressWarnings("unchecked")
        public static <T> List<T> parseToListOfObjects(FileToParse file){
        Reader reader = new InputStreamReader(Objects.requireNonNull(JSONParser.class.getClassLoader().
                getResourceAsStream(file.fileName)), StandardCharsets.UTF_8);
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(reader, new ListWrapper<T>(file.returnedClass));
      }


      public enum FileToParse{
          DEFAULT_CLASSIFICATION("DefaultClassification.json", ClassificationObject.class),
          SEMANTIC_TYPES("SemanticTypes.json",PIIType .class);


          String fileName;
          Class returnedClass;

          FileToParse(String fileName,Class returnedClass){
              this.fileName = fileName;
              this.returnedClass = returnedClass;
          }

      }

}
