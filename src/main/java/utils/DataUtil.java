package utils;

import org.apache.commons.lang.RandomStringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DataUtil {

    public static String randomString(int length){
        return RandomStringUtils.randomAlphabetic(5);
    }

    public static String currentDate(){
        return LocalDateTime.now().format( DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss"));
    }

    public static String underscoreDate(){
        return LocalDateTime.now().format( DateTimeFormatter.ofPattern("yyyy_MM_dd HH_mm_ss"));
    }

    public static String serverDateTime(){
        LocalDateTime time = (System.getProperty("os.name")).startsWith("Linux") ? LocalDateTime.now().minusHours(3):LocalDateTime.now();
        return time.format(DateTimeFormatter.ofPattern("MMM d, yyyy H_mm_ss"));
    }

    public static String underscoreDate(String string){
        SimpleDateFormat oldFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat newFormat = new SimpleDateFormat("MMM d, yyyy H_mm_ss");
        Date date;
        try {
            date = oldFormat.parse(string.trim());
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
