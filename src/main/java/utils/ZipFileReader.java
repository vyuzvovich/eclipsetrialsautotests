package utils;

import businessobjects.csvmapping.CSVFile;
import core.driver.DriverManager;
import core.logger.Logger;
import waiters.Waiters;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipFileReader {
    private static final int BUFFER_SIZE = 8192;

    public ZipFileReader(String path) {
        zipFile =  Arrays.stream(getPossiblePath(path)).map(this::getZipByPath).filter(s -> s!=null).findFirst().get();
    }

    private ZipFile zipFile;

    public CSVFile[] getCSVFilesFromZip() {
        Enumeration<? extends ZipEntry> entries = this.zipFile.entries();
        List<CSVFile> files = new ArrayList<>();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (entry.getName().endsWith(".csv")) {
                try {
                    files.add(new CSVFile(extractEntry(entry, zipFile.getInputStream(entry))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return files.toArray(new CSVFile[files.size()]);
    }

    private static String extractEntry(final ZipEntry entry, InputStream is) {
        String exractedFile = DriverManager.MOUNT_DIR + entry.getName();
        Logger.info("Extract entry from zip file "+ exractedFile);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(exractedFile);
            final byte[] buf = new byte[BUFFER_SIZE];
            int read = 0;
            int length;
            while ((length = is.read(buf, 0, buf.length)) >= 0) {
                fos.write(buf, 0, length);
            }
        } catch (IOException ioex) {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return exractedFile;
    }

    private String getPathSubstring(String path, int lastSecond){
        return path.substring(0, path.length()-5)+ lastSecond + path.substring(path.length()-4);
    }

    private String[] getPossiblePath(String path){
        String lastSecond = path.split("\\.")[0].substring(path.split("\\.")[0].length()-1);
        String[] result = {path, getPathSubstring(path, Integer.parseInt(lastSecond)+1), getPathSubstring(path, Integer.parseInt(lastSecond)-1)};
        return result;
    }

    private ZipFile getZipByPath(String path) {
        Logger.info("Get Zip file by path "+path);
        for (int i = 0; i < 3; i++) {
            try {
                zipFile = new ZipFile(path);
            } catch (IOException | NullPointerException e) {
                Logger.debug("Catch FileNotFoundException");
                Waiters.threadSleep(1);
            }
            if (zipFile!=null)
                break;
            }
        return zipFile;
    }
}
