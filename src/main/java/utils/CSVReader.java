package utils;

import businessobjects.ClassificationObject;
import businessobjects.csvmapping.CSVFile;
import core.logger.Logger;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.Modifier;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;

import static javassist.ClassPool.getDefault;

public class CSVReader {

    public CSVReader(CSVFile file, Map<String, String> customFieldsMap) {
        records = parseCSV(file.getFilePath());
        String className = (file.getName().contains("_deid"))? file.getName().split("_deid")[0] : file.getName().split("\\.")[0];
        fieldToTypeMap = (customFieldsMap==null) ? setFields() : setCustomFields(customFieldsMap);
        csvClass = generateClass(className, records.get(0));
    }

    private LinkedHashMap<String, String> setFields() {
        LinkedHashMap<String, String> fields = new LinkedHashMap<>();
        records.get(0).forEach(f -> fields.put(f, classifications.stream().filter(c -> c.getFieldName().
                equals(f)).findFirst().get().getJavaType()
        ));
        return fields;
    }

    private LinkedHashMap<String, String> setCustomFields(Map<String, String> customFieldsMap) {
        LinkedHashMap<String, String> fields = setFields();
        for(Map.Entry<String, String> entry : customFieldsMap.entrySet()){
            if(fields.containsKey(entry.getKey())){
                fields.replace(entry.getKey(), entry.getValue());
            }
        }
        return fields;
    }

    private List<List<String>> records;
    private Class csvClass;
    private static List<ClassificationObject> classifications = ClassificationObject.getDefaultClassification();
    private LinkedHashMap<String, String> fieldToTypeMap;


    @SuppressWarnings("unchecked")
    public <csvClass> List<csvClass>  parseCSVToObjects(String dateFormatToOverwrite) {
        List<csvClass> objects = new LinkedList<>();
        for (int i = 1; i < records.size(); i++) {
            csvClass instance = null;
            try {
                instance = (csvClass) csvClass.newInstance();
                for (Field field : instance.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    String[] dateFormats = classifications.stream().filter(c -> c.getFieldName().equals(field.getName())).findFirst().get().getDateFormat();
                    int fieldPosition = new ArrayList<>(fieldToTypeMap.keySet()).indexOf(field.getName());
                    String fieldValue = records.get(i).get(fieldPosition);
                    field.set(instance, convertToType(fieldToTypeMap.get(field.getName()), fieldValue, dateFormats, dateFormatToOverwrite));
                }
            } catch (InstantiationException | IllegalAccessException | IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            objects.add(instance);
        }
        return objects;
    }

    public <csvClass> List<csvClass> parseCSVToObjects(){
        return parseCSVToObjects(null);
    }

    @SuppressWarnings("unchecked")
    private <T> T convertToType(String type, String fieldValue, String[] dateFormats, String dateFormatToOverwrite) {
        Class<T> clazz = null;
        try {
            clazz = (Class<T>) Class.forName(type);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        assert clazz != null;
        T value = null;
        switch (type) {
            case ("java.lang.String"):
                value = clazz.cast(fieldValue);
                break;
            case ("java.lang.Long"):
                try {
                    long v = Long.parseLong(fieldValue);
                    value = clazz.cast(v);
                    break;
                } catch (NumberFormatException nfex){
                    Logger.warn("Value '"+fieldValue +"' fail to be casted to Long");
                   break;
                }
            case ("java.lang.Double"):
                try {
                    value = clazz.cast(Double.parseDouble(fieldValue));
                    break;
                } catch (NumberFormatException nfex){
                    Logger.warn("Value '"+fieldValue +"' fail to be casted to Double");
                    break;
                }
            case ("java.util.Date"):
                Date date = null;
                try {
                    if(dateFormatToOverwrite!=null){
                        date = new SimpleDateFormat(dateFormatToOverwrite).parse(fieldValue);
                        Logger.debug(fieldValue + " parsed successfully with " + dateFormatToOverwrite);
                    }
                    else {
                        for (String dateFormat : dateFormats) {
                            try {
                                date = new SimpleDateFormat(dateFormat).parse(fieldValue);
                                Logger.debug(fieldValue + " parsed successfully with " + dateFormat);
                                break;
                            } catch (ParseException e) {
                                Logger.debug(e.getMessage() + "for " + fieldValue + " parsing with " + dateFormat);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.error(e.getMessage() + "for " + fieldValue + " parsing");
                }
                value = clazz.cast(date);
                break;
        }
        return value;
    }

    private List<List<String>> parseCSV(String path) {
        BufferedReader br = new BufferedReader(Objects.requireNonNull(getFileReader(path)));
        List<List<String>> records = new ArrayList<>();
        String line;
        try {
            while ((line = br.readLine()) != null) {
                ArrayList<String> columnList = parseColumnList(line);
                records.add(columnList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return records;
    }

    private ArrayList<String> parseColumnList(String line) {
        String[] splitedText = line.split(",");
        ArrayList<String> columnList = new ArrayList<>();
        for (int i = 0; i< splitedText.length; i++) {
            if (!isColumnStart(splitedText[i])) {
                columnList.add(splitedText[i]);
            } else {
                int j = 1;
                String concatStr = splitedText[i];
                while (!isColumnEnd(splitedText[i + j])) {
                    concatStr = concatStr + splitedText[i + j];
                    j++;
                }
                columnList.add(concatStr);
                i+=j;
            }
        }
        return columnList;
    }

    private boolean isColumnStart(String str) {
        String trimText = str.trim();
        return trimText.startsWith("\"") &&  !trimText.endsWith("\"");//TODO check if this enough, if \"text" sometext\ possib;e

    }
    private boolean isColumnEnd(String str) {
        String trimText = str.trim();
        return !trimText.startsWith("\"") &&  trimText.endsWith("\"");//TODO check if this enough, if \ sometext "text"\ possib;e

    }

    private Class generateClass(String className, List<String> fields) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException ex) {
            ClassPool pool = getDefault();
            CtClass cc = pool.makeClass(className);
            fields.forEach(f -> {
                try {CtField field = new CtField(pool.get(fieldToTypeMap.get(f)), f, cc);
                    field.setModifiers(Modifier.PUBLIC);
                    cc.addField(field);
                } catch (CannotCompileException | javassist.NotFoundException | NoSuchElementException e) {
                    Logger.error("No such element is mapped: " + f + ". Check json and add appropriate node!");
                    e.printStackTrace();
                }
            });
            try {
                return cc.toClass();
            } catch (CannotCompileException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static FileReader getFileReader(String file) {
        try {
            return new FileReader(new File(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
