package pages;

import businessobjects.enums.Users;
import core.htmlelementswrapper.Button;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.blocks.AddUserPopUp;
import pages.blocks.UserRow;
import waiters.Waiters;

import java.util.List;

public class UserManagementPage extends AdminPanelPage {

    @FindBy(xpath = "//button[@ng-click='vm.CreateUser()']")
    private Button createUserBtn;

    private AddUserPopUp addUserPopUp;

    private List<UserRow> userRowList;

    @Override
    public Page getPage() {
        return Page.ADMIN_PANEL_PAGE;
    }


    public void createUser(Users user) {
        Logger.info("Create user: " + user.getName());
        createUserBtn.click();
        addUserPopUp.addUser(user);
    }

    public boolean verifyUserExists(Users user) {
        Waiters.waitForListOfElementsToDisplay(userRowList, 10);
        if (userRowList.isEmpty()) {
            return false;
        }
        userRowList.get(0).waitForAnimationComplete();
        for (UserRow row : userRowList) {
            if (row.containsUsername(user.getName())) {
                return true;
            }
        }
        return false;
    }
}
