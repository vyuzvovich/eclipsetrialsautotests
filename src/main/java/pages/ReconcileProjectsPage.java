package pages;

import businessobjects.ClassificationTable;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.DropDown;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.blocks.ThreeSetTable;
import waiters.Waiters;

import java.util.Map;
import java.util.NoSuchElementException;


public class ReconcileProjectsPage extends ContentPage {

    @FindBy(xpath = "//*[@ng-class=\"{'disabled-item': vm.projectTables.length == 0}\"]")
    private ThreeSetTable currentProjectTablesAndColumns;

    @FindBy(xpath = "//*[@ng-class=\"{'disabled-item': vm.templateTables.length == 0}\"]")
    private ThreeSetTable templateTables;

    @FindBy(xpath = "//select[@ng-change=\"vm.selectTemplate()\"]")
    private DropDown selectedTemplate;

    @FindBy(xpath = "//button[@ng-click=\"vm.finished()\"]")
    private Button continueButton;

    @FindBy(xpath = "//button[@ng-click=\"vm.matchTables()\"]")
    private Button matchChosenTables;


    @Override
    public Page getPage() {
        return Page.RECONCILE_PROJECTS_PAGE;
    }

    public DataModelPage navigateToDataModel() {
        continueButton.click();
        return new DataModelPage();
    }

    public ClassificationPage navigateToClassificationPage() {
        continueButton.click();
        return new ClassificationPage();
    }

    public void matchChosenTables(String templateName, Map<ClassificationTable, ClassificationTable> matchingMap) {
        Waiters.waitForURLContains(Page.RECONCILE_PROJECTS_PAGE.getLink(), 30);
        Waiters.waitForSpinnerDisable();//TODO: remove after performance improves

        selectedTemplate.waitForClickable();
        selectedTemplate.waitForAnimationComplete();
        selectedTemplate.click();
        selectedTemplate.waitForAnimationComplete();
        selectedTemplate.getOptions().stream().filter(o -> o.getText().equals(templateName)).findFirst().orElseThrow(NoSuchElementException::new);

        selectedTemplate.selectByVisibleText(templateName);
        templateTables.waitForDisplayed();
        for (Map.Entry<ClassificationTable, ClassificationTable> entry : matchingMap.entrySet()) {
            if (entry.getValue().equals(null)) {
                Logger.warn("Table '" + entry.getKey() + "' has no matching in template");
                continue;
            }
            currentProjectTablesAndColumns.selectTableByName(entry.getKey().getNameWithoutExtension());
            templateTables.selectTableByName(entry.getValue().getNameWithoutExtension());
            matchChosenTables.waitForClickable();
            matchChosenTables.click();
            Logger.info("Table '" + entry.getKey() + "' matched with '" + entry.getValue() + "'");
        }
    }

}
