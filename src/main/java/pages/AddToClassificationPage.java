package pages;

import businessobjects.ClassificationTable;
import businessobjects.DataSource;
import core.htmlelementswrapper.Button;
import org.openqa.selenium.support.FindBy;
import pages.blocks.ThreeSetTable;
import waiters.Waiters;


public class AddToClassificationPage extends ContentPage{

    @FindBy(xpath = "//*[@ng-class=\"{'disabled-item': !vm.selectedDB}\"]")
    private ThreeSetTable availableTables;

    @FindBy(xpath ="//*[@ng-class=\"{'disabled-item': vm.tablesAndColumns.length == 0}\"]")
    private ThreeSetTable selectedTables;

    @FindBy(xpath = "//button[@ng-click=\"vm.createModel('model')\"]")
    private Button continueButton;

    @FindBy(xpath = "//button[@ng-click=\"vm.createModel('locate.compare')\"]")
    private Button applyATemplateBtn;

    public boolean verifyTablesAvailable(DataSource dataSource) {
        return availableTables.containsTables(dataSource.getClassificationTables(), true);
    }

    public boolean verifyColumns(DataSource dataSource){
        boolean result = true;
        for (ClassificationTable table:dataSource.getClassificationTables()) {
            availableTables.selectTable(table);
            selectedTables.waitForAnimationComplete();
            result &= selectedTables.verifyTableColumns(table);
        }
        return result;
    }

    public boolean selectAllColumns(DataSource dataSource){
        boolean result = true;
        for (ClassificationTable table:dataSource.getClassificationTables()) {
            availableTables.selectTable(table);
            selectedTables.waitForAnimationComplete();
            result &= selectedTables.verifyTableColumns(table);
        }
        return result;
    }

    @Override
    public Page getPage() {
        return Page.ADD_TO_CLASSIFICATION_PAGE;
    }

    public DataModelPage navigateToDataModel() {
        continueButton.click();
        return new DataModelPage();
    }

    public ClassificationPage navigateToClassificationPage(){
        continueButton.click();
        return new ClassificationPage();
    }

    public ReconcileProjectsPage clickApplyATemplateBtn(){
        applyATemplateBtn.click();
        Waiters.waitForURLContains(Page.RECONCILE_PROJECTS_PAGE.getLink(), 30);
        Waiters.waitForSpinnerDisable();
        return new ReconcileProjectsPage();
    }
}
