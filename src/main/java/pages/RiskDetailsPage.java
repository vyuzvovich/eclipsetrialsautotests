package pages;

import core.htmlelementswrapper.Button;
import org.openqa.selenium.support.FindBy;

public class RiskDetailsPage extends ContentPage{

    @FindBy(xpath = "//button[@ng-click = 'vm.ToDeidentify()']" )

    private Button continueButton;

    @Override
    public Page getPage() {
        return Page.RISK_DETAILS_PAGE;
    }

    public DirectIdentifiersPage navigateToDirectIdentifiers(){
        continueButton.click();
        return new DirectIdentifiersPage();
    }
}
