package pages;

import core.driver.DriverManager;
import core.driver.PropertyLoader;
import core.logger.Logger;
import org.openqa.selenium.WebDriver;
import pages.blocks.ModalWindow;
import pages.blocks.TopMenu;
import pages.blocks.UserSubMenu;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import waiters.Waiters;

public abstract class ContentPage{

    private TopMenu menu;

    private UserSubMenu userSubMenu;

    ModalWindow modalWindow;

    ContentPage(){ HtmlElementLoader.populatePageObject(this, driver);}

    private WebDriver driver = DriverManager.getDriver();

    public TopMenu getMenu() { return menu; }

    public abstract Page getPage();

    public boolean checkPageIsLoaded(){
        Logger.info("Check page " + getPage().getName() + " is loaded");
        Waiters.waitForURLContains(getPage().getLink(), 60);
        return DriverManager.getDriver().getCurrentUrl().contains(getPage().getLink());
    }

    public static String getCurrentUrl(){
        return DriverManager.getDriver().getCurrentUrl();
    }

    @SuppressWarnings("unchecked")
    public <T extends ContentPage> T openPage(){
        Logger.info("Open '" + getPage().getName() + "' Page");
        DriverManager.getDriver().get(PropertyLoader.get(PropertyLoader.Property.APP_URL)+getPage().getLink());
        Waiters.waitForAnimationCompleteOnPage(5);
        try {
            return (T) Class.forName(this.getClass().getName()).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public UserSubMenu getUserSubMenu() {
        return userSubMenu.hoverUserSubmenuTitle();
    }

}
