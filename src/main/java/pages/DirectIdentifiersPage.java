package pages;

import businessobjects.DataSource;
import businessobjects.enums.Masking;
import core.htmlelementswrapper.Button;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.blocks.DIMaskingRow;
import waiters.Waiters;
import java.util.List;

public class DirectIdentifiersPage extends ContentPage{

    private List<DIMaskingRow> maskingRows;

    @FindBy(xpath = "//button[@ng-click = 'vm.Continue()']")
    private Button continueButton;

    @FindBy(xpath = "//button[@ng-click = 'vm.LoadDefaults()']")
    private Button loadDefaultsBtn;

    @Override
    public Page getPage() { return Page.DIRECT_IDENTIFIERS_PAGE;}

    public DirectIdentifiersPage addMasking(DataSource dataSource, String field, Masking.DIRECT_IDENTIFIERS masking) {
        Logger.info("Add masking " + masking + " to field: " + field);
        Waiters.waitForListOfElementsToDisplay(maskingRows, 5);
        maskingRows.stream().filter(c ->c.getField().getText().equals(field)).findFirst().get().setMasking(masking);
        dataSource.addMaskingItem(field, masking);
        return this;
    }

    public DirectIdentifiersPage addDefaultMasking(){
        Logger.info("Add default masking to field with masking: " + DIMaskingRow.PLEASE_SPECIFY);
        Waiters.waitForListOfElementsToDisplay(maskingRows, 5);
        maskingRows.stream().filter(c ->c.getMaskingDetails().getText().equals(DIMaskingRow.PLEASE_SPECIFY)).forEach(DIMaskingRow::setDefaultMasking);
        return this;
    }

    public DirectIdentifiersPage clickLoadDefaults(){
        Waiters.waitForListOfElementsToDisplay(maskingRows, 15);
        Logger.info("Click Load Defaults button for Direct Identifiers page");
        loadDefaultsBtn.waitForClickable();
        loadDefaultsBtn.click();
        return this;
    }
    public QuasiIdentifiersPage navigateToQuasiIdentifiers(){
        continueButton.click();
        return new QuasiIdentifiersPage();
    }
}
