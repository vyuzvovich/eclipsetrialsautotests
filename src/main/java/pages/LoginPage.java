package pages;

import businessobjects.enums.Users;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.TextInput;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import waiters.Waiters;

public class LoginPage extends ContentPage {

    @FindBy(name = "userName")
    private TextInput userName;

    @FindBy(name = "password")
    private TextInput password;

    @FindBy(name = "verifyPassword")
    private TextInput verifyPassword;

    @FindAll({
            @FindBy(xpath = "//button[@type='submit']"),
            @FindBy(xpath = "//button[contains(@ng-click, 'vm.SignUp()')]")
    })
    private Button submit;

    private Users defaultUser = Users.FIRST_ADMIN;

    /**
     * Login with {@link businessobjects.enums.Users#FIRST_ADMIN}
     */
    public void loginWithDefaultUser() {
        loginWithUser(defaultUser);
    }

    /**
     * Login with specified user (already created)
     *
     * @param user from {@link businessobjects.enums.Users}
     */
    public void loginWithUser(Users user) {
        userName.clear();
        userName.type(user.getName());
        password.clear();
        password.type(user.getPassword());
        submit.waitForClickable();
        submit.click();
        Waiters.waitForSpinnerDisable(); //TODO: remove after build performance problems disappear
        Waiters.waitForSpinnerDisable();
        Waiters.waitForPageLoaded(Page.HOME_PAGE.getLink(), 60);
        Waiters.waitForSpinnerDisable();
    }

    /**
     * Create "main" account for the First User (first after application deployment)
     *
     * @param user from {@link Users}
     */
    public void createAccount(Users user) {
        userName.type(user.getName());
        password.type(user.getPassword());
        verifyPassword.type(user.getPassword());
        submit.waitForDisplayed();
        submit.click();
        submit.waitForAnimationComplete();
    }

    /**
     * Login as specified user: if account is already created, user is navigated to Home page
     *
     * @param user from {@link Users}
     * @return true if user is navigated to Home page, false if not
     */
    public boolean verifyAccountIsCreated(Users user) {
        loginWithUser(user);
        return new HomePage().checkPageIsLoaded();
    }

    /**
     * Checks, if the Login page is a "First User Creation" page
     *
     * @return true, if verifyPassword is displayed, false if not
     */
    public boolean isFirstUserCreationScreen() {
        try {
            verifyPassword.isDisplayed();
            return true;
        } catch (NoSuchElementException nex) {
            return false;
        }
    }


    @Override
    public Page getPage() {
        return Page.LOGIN_PAGE;
    }

}
