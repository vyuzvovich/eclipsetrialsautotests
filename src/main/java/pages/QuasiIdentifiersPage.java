package pages;

import businessobjects.DataSource;
import businessobjects.enums.QuasiIdentifiers;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.blocks.QIMaskingRow;
import waiters.Waiters;

import java.util.List;

public class QuasiIdentifiersPage extends ContentPage {

    private List<QIMaskingRow> maskingRows;

    @FindBy(xpath = "//button[@ng-click = 'vm.Continue()']")
    private Button continueButton;

    @FindBy(xpath = "//a[@dropdown-toggle=\"#table-functions\"]")
    private Element globalActions;

    @FindBy(xpath = "//a[@ng-click=\"vm.loadDefaults()\"]")
    private Element loadDefaults;

    public QuasiIdentifiersPage addMasking(DataSource dataSource, String field, QuasiIdentifiers masking) {
        Logger.info("Add masking " + masking + " to field: " + field);
        Waiters.waitForListOfElementsToDisplay(maskingRows, 5);
        maskingRows.stream().filter(c -> c.getField().getText().equals(field)).findFirst().get().setMasking(masking);
        dataSource.addMaskingItem(field, masking);
        return this;
    }

    public QuasiIdentifiersPage addDefaultMasking() {
        Logger.info("Add default masking to field with default masking: " + QIMaskingRow.PLEASE_SPECIFY);
        Waiters.waitForListOfElementsToDisplay(maskingRows, 5);
        maskingRows.stream().filter(c -> c.getMaskingDetails().getText().equals(QIMaskingRow.PLEASE_SPECIFY)).forEach(QIMaskingRow::setDefaultMasking);
        return this;
    }

    @Override
    public Page getPage() {
        return Page.QUASI_IDENTIFIERS_PAGE;
    }

    public DeIdentityPage navigateToDestinationPage() {
        continueButton.waitForAnimationComplete();
        continueButton.click();
        return new DeIdentityPage();
    }

    public void clickGlobalActionsLink() {
        globalActions.click();
    }

    public void loadDefaults() {
        Waiters.waitForListOfElementsToDisplay(maskingRows, 15);
        Logger.info("Click Load Defaults link for Quasi-Identifiers page");
        clickGlobalActionsLink();
        loadDefaults.waitForClickable();
        loadDefaults.click();

    }
}
