package pages;

import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.DropDown;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import waiters.Waiters;

public class DeIdentityPage extends ContentPage{

    @FindBy(xpath = "//button[@ng-click='vm.RunMasking()']")
    private Button runDeIdentification;

    @FindBy(xpath = "//select[@ng-change=\"vm.showSuffixErrorIfNeeded()\"]")
    private DropDown exportSettings;

    @FindBy(xpath = "//option[@value=\"string:always\"]")
    private Element always;

    @FindBy(xpath = "//option[@value=\"string:lowrisk\"]")
    private Element lowrisk;

    @FindBy(xpath = "//option[@value=\"string:never\"]")
    private Element never;


    //TODO: think, if needed to be moved
    public static final String ALWAYS = "string:always";
    public static final String LOWRISK = "string:lowrisk";
    public static final String NEVER = "string:never";

    public HomePage deIdentifyWithDefaultSettings(){
        Waiters.waitForURLContains(Page.DE_IDENTITY_PAGE.getLink(), 30);
        Waiters.waitForSpinnerDisable();
        runDeIdentification.waitForAnimationComplete();
        runDeIdentification.click();
        runDeIdentification.waitForAnimationComplete();
        return new HomePage();
    }

    @Override
    public Page getPage() {
        return Page.DE_IDENTITY_PAGE;
    }


    /**
     * This method fills DeIdentify page with settings and clicks Run De-Identification button
     * @param exportValue should be DeIdentityPage.#ALWAYS, DeIdentityPage.LOWRISK or DeIdentityPage.NEVER
     * @return {@link HomePage}
     */
    public HomePage deIdentifyWithExportSettings(String exportValue){
        Waiters.waitForURLContains(Page.DE_IDENTITY_PAGE.getLink(), 30);
        Waiters.waitForSpinnerDisable();

        exportSettings.waitForAnimationComplete();
        Logger.debug("Export settings options count = "+ exportSettings.getOptions().size());
        exportSettings.waitForClickable();
        exportSettings.click();
        //TODO: re-check after performance issues gone
        try{
            exportSettings.selectByValue(exportValue);
            Logger.info("Selected option: "+exportSettings.getFirstSelectedOption().getText());

            if(!exportSettings.getFirstSelectedOption().getAttribute("value").equals(exportValue)){
                Logger.warn("Failed to select by htmlelements Select value "+exportValue +". Try another");
                selectExportSettingsAlternative(exportValue);
            }

        } catch (Exception ex){
            Logger.warn("Failed to select by htmlelements Select: " + ex.getMessage() + ". Try another");
            selectExportSettingsAlternative(exportValue);
        }
        runDeIdentification.waitForAnimationComplete();
        runDeIdentification.waitForClickable();
        if(!runDeIdentification.isEnabled()){
            exportSettings.click();
            exportSettings.selectByValue(exportValue);
            if(!exportSettings.getFirstSelectedOption().getAttribute("value").equals(exportValue)){
                Logger.warn("Failed to select by htmlelements Select value "+exportValue +". Try another");
                selectExportSettingsAlternative(exportValue);
            }
        }
        runDeIdentification.click();
        HomePage homePage = new HomePage();
        Waiters.waitForURLContains(Page.HOME_PAGE.getLink(), 480);
        Waiters.waitForSpinnerDisable();
        return  homePage;
    }

    private void selectExportSettingsAlternative(String exportValue) {
        switch (exportValue) {
            case DeIdentityPage.ALWAYS:
                always.click();
                break;
            case DeIdentityPage.LOWRISK:
                lowrisk.click();
                break;
            case DeIdentityPage.NEVER:
                never.click();
                break;
        }
        Logger.info("Selected option: "+exportSettings.getFirstSelectedOption().getText());
    }
}
