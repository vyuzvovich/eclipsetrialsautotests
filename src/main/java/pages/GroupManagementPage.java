package pages;

import businessobjects.User;
import businessobjects.enums.Users;
import core.logger.Logger;
import pages.blocks.EditGroupPopUp;
import pages.blocks.UserGroupRow;
import waiters.Waiters;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GroupManagementPage extends AdminPanelPage {

    private List<UserGroupRow> userGroupRows;

    @Override
    public Page getPage() {
        return Page.GROUP_MANAGEMENT_PAGE;
    }

    @Deprecated
    public void addUserToGroups(User user) {
        for (User.UserGroup group : user.getUserGroups()) {
            Logger.info("Add user to group: " + group.getName());
            userGroupRows.get(0).waitForAnimationComplete();
            UserGroupRow row = userGroupRows.stream().filter(r -> r.getUserGroupTitle().getText().equals(group.getName())).findFirst().get();
            row.editGroup().addUserToGroup(user);
        }
    }

    /**
     * Method sets groups for User according to roles
     *
     * @param user references {@link businessobjects.enums.Users Users enum member}
     * @return boolean if user added to groups successfully
     */
    public boolean addUserToGroups(Users user) {
        boolean result = true;
        Waiters.waitForListOfElementsToDisplay(userGroupRows, 5);
        LinkedHashMap<Users.UserGroup, Users.UserLevel> groupUserLevelMap = (LinkedHashMap<Users.UserGroup, Users.UserLevel>) user.getGroupManagementMap();
        for (Map.Entry<Users.UserGroup, Users.UserLevel> entry : groupUserLevelMap.entrySet()) {
            Users.UserGroup group = entry.getKey();
            Users.UserLevel level = entry.getValue();
            Waiters.waitForListOfElementsToDisplay(userGroupRows, 30);
            Waiters.waitForElementToDisplay(userGroupRows.get(2), 30);
            userGroupRows.get(0).waitForAnimationComplete();

            UserGroupRow row = userGroupRows.stream().filter(r -> r.getUserGroupTitle().getText().equals(group.getGroupName())).findFirst().get();
            if (level.equals(Users.UserLevel.ADMINS) && row.getAdminsTitle().getText().contains(user.getName())) {
                Logger.info("User '" + user.getName() + "' already have the role " + group + "-" + level);
                continue;
            } else if (level.equals(Users.UserLevel.MEMBERS) && row.getMembersTitle().getText().contains(user.getName())) {
                Logger.info("User '" + user.getName() + "' already have the role " + group + "-" + level);
                continue;
            }
            Logger.info("Add user '" + user.getName() + "' to: " + group + "-" + level);
            EditGroupPopUp editGroupPopUp = row.editGroup();
            editGroupPopUp.addUserToGroupWithLevel(user, level);
            userGroupRows.get(3).waitForAnimationComplete();
            Waiters.threadSleep(5); //TODO remove after performance issue fixed
            row = userGroupRows.stream().filter(r -> r.getUserGroupTitle().getText().equals(group.getGroupName())).findFirst().get();
            if (level.equals(Users.UserLevel.ADMINS) && !row.getAdminsTitle().getText().contains(user.getName())) {
                result = false;
            } else if (level.equals(Users.UserLevel.MEMBERS) && !row.getMembersTitle().getText().contains(user.getName())) {
                result = false;
            }
        }
        return result;
    }


}



