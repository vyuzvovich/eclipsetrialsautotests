package pages;

import pages.blocks.AdminPageTabMenu;

public class AdminPanelPage extends ContentPage {

    private AdminPageTabMenu adminPageTabs;

    @Override
    public Page getPage() {
        return Page.ADMIN_PANEL_PAGE;
    }

    public AdminPageTabMenu getAdminPageTabs(){
        return adminPageTabs;
    }
}
