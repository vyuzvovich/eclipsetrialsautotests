package pages;

import core.htmlelementswrapper.Button;
import org.openqa.selenium.support.FindBy;
import waiters.Waiters;

public class DataGroupsPage extends ContentPage{

    @FindBy(xpath = "//button[@ng-click ='vm.ToContinue()']")
    private Button continueButton;

    @Override
    public Page getPage() {
        return Page.DATA_GROUPS_PAGE;
    }

    public ContextPage navigateToContextPage(){
        continueButton.click();
        Waiters.waitForSpinnerDisable();
        return new ContextPage();
    }
}
