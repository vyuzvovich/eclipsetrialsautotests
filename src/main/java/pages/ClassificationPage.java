package pages;

import businessobjects.ClassificationTable;
import businessobjects.DataModel;
import businessobjects.DataSource;
import businessobjects.enums.DataTypes;
import core.htmlelementswrapper.Button;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.blocks.ClassificationRow;
import waiters.Waiters;

import java.util.List;

public class ClassificationPage extends ContentPage {

    private List<ClassificationRow> classificationRows;

    @FindBy(xpath = "//button[@ng-click = 'vm.goForward()']")
    private Button continueButton;

    @Override
    public Page getPage() {
        return Page.CLASSIFICATION_PAGE;
    }

    public ClassificationPage addDefaultClassifications() {
        Waiters.waitForURLContains(ClassificationPage.getCurrentUrl(), 30);
        Waiters.waitForListOfElementsToDisplay(classificationRows, 60);
        Waiters.waitForSpinnerDisable();
        Logger.info("Add default classification to field with PII type: " + ClassificationRow.UNKNOWN);
        classificationRows.stream().filter(c -> c.getPIIType().getText().equals(ClassificationRow.UNKNOWN)).forEach(ClassificationRow::setDefaultPIIType);
        classificationRows.stream().filter(c -> c.getClassification().getText().equals(ClassificationRow.PLEASE_CLASSIFY)).forEach(ClassificationRow::setDefaultClassification);
        Waiters.waitForSpinnerDisable();
        if (!continueButton.isEnabled()) {
            Logger.warn("Continue Button is disabled. Try to fill Unknow again, if not filled");
            classificationRows.stream().filter(c -> c.getPIIType().getText().equals(ClassificationRow.UNKNOWN)).forEach(ClassificationRow::setDefaultPIIType);
            classificationRows.stream().filter(c -> c.getClassification().getText().equals(ClassificationRow.PLEASE_CLASSIFY)).forEach(ClassificationRow::setDefaultClassification);
        }
        return this;
    }

    public ClassificationPage addClassification(String field, String PIIType, DataTypes dataType) {
        Logger.info("Add Classification to field: " + field + ". Set PII type " + PIIType + ". Set DataType " + dataType.toString());
        classificationRows.stream().filter(c -> c.getField().getText().equals(field)).findFirst().get().setPIIType(PIIType).setDataType(dataType.toString());
        return this;
    }

    public boolean verifyClassificationLoaded(DataSource dataSource) {
        Logger.info("Verify tables are classified correctly");
        Waiters.waitForListOfElementsToDisplay(classificationRows, 15);
        boolean result = true;
        for (ClassificationTable table : dataSource.getClassificationTables()) {
            DataModel model = (dataSource.getDataModels().size() > 1) ? dataSource.getDataModels().stream().filter(d -> d.getParentTable()
                    .getName().equals(table.getName()) || d.getChildTable().getName().equals(table.getName())).findFirst().get() : null;
            Waiters.waitForListOfElementsToDisplay(classificationRows, 2);
            result &= classificationRows.stream().anyMatch(c -> c.matchesTable(table, model));
        }
        return result;
    }

    public DataGroupsPage navigateToDataGroups() {
        continueButton.click();
        return new DataGroupsPage();
    }
}
