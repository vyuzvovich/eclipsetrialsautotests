package pages;

import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.DropDown;
import core.htmlelementswrapper.TextInput;
import org.openqa.selenium.support.FindBy;
import waiters.Waiters;

public class ContextPage extends ContentPage {

    @FindBy(xpath = "//button[@click-and-disable = 'vm.RunRiskMeasurement()']")
    private Button measureRiskButton;

    @FindBy(xpath = "//select[@ng-model=\"vm.dataDetails.populationItem\"]")
    private DropDown populationSelect;

    @FindBy(xpath = "//input[@name='population']")
    private TextInput populationField;

    public static final String CUSTOM = "Custom";

    @Override
    public Page getPage() {
        return Page.CONTEXT_PAGE;
    }

    public HomePage measureRisk() {
        Waiters.waitForSpinnerDisable();
        measureRiskButton.waitForAnimationComplete();
        measureRiskButton.waitForClickable();
        Waiters.threadSleep(30);//TODO: REMOVE after check!

        measureRiskButton.waitForClickable();
        measureRiskButton.click();
        measureRiskButton.waitForAnimationComplete();
        //measureRiskButton.waitForNotDisplayed();
        HomePage homePage = new HomePage();
        Waiters.waitForURLContains(HomePage.getCurrentUrl(), 240);
        return homePage;
    }

    public void setCustomPopulation(int customValue){
        populationSelect.selectByVisibleText(CUSTOM);
        populationField.waitForDisplayed();
        populationField.clear();
        populationField.type(String.valueOf(customValue));
    }
}
