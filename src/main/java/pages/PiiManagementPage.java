package pages;

import businessobjects.PIIType;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.Link;
import core.htmlelementswrapper.TextInput;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.blocks.AddPIIForm;
import waiters.Waiters;

import java.util.List;

public class PiiManagementPage extends AdminPanelPage {

    @FindBy(xpath = "//button[@ng-click='vm.addPII(piiForm)']")
    private Button createPiiBtn;

    private AddPIIForm addPIIForm;

    @FindBy(xpath = "//input[@type='search']")
    private TextInput searchField;

    @FindBy(xpath = "//ul[@id='searchTextResults']//li[1]")
    private List<Element> piiList;

    @FindBy(xpath = "//img")
    private Element image;

    @FindBy(xpath = "//ul[@id='searchTextResults']/a[1]")
    private Link firstListLink;


    @Override
    public Page getPage() {
        return Page.PII_TYPE_SETTINGS;
    }


    public PiiManagementPage createPII(PIIType customPiiType) {
        Logger.info("Create PII: " + customPiiType.getName());
        firstListLink.waitForClickable();
        firstListLink.waitForAnimationComplete();
        createPiiBtn.click();
        addPIIForm.waitForAnimationComplete();
        addPIIForm.addPiiType(customPiiType);
        getFirstElementOfPiiList().waitForAnimationComplete();
        Waiters.waitForAnimationCompleteOnPage(5);
        return new PiiManagementPage();
    }

    public PiiManagementPage editPII(String previousName, PIIType customPiiType) {
        Logger.info("Edit PII: " + previousName);
        if (!previousName.equals(customPiiType.getName())) {
            Logger.info("Rename Pii to " + customPiiType.getName());
        }
        firstListLink.waitForClickable();
        firstListLink.waitForAnimationComplete();
        filterPii(previousName);
        getFirstElementOfPiiList().waitForAnimationComplete();
        Element firstRow = getFirstElementOfPiiList();
        firstRow.getWrappedElement().click();

        addPIIForm.waitForAnimationComplete();
        addPIIForm.editPiiType(customPiiType);
        Waiters.waitForAnimationCompleteOnPage(5);
        return new PiiManagementPage();
    }

    public int filterPii(String keyName) {
        searchField.waitForAnimationComplete();
        searchField.clear();
        searchField.type(keyName);
        Waiters.waitForAnimationCompleteOnPage(50);
        if (piiList.isEmpty()) {
            Logger.info("No issues filtered!");
            return 0;
        }
        return piiList.size();
    }


    public Element getFirstElementOfPiiList() {
        return piiList.get(0);
    }

    public boolean verifyPIIExists(PIIType piiType) {
        searchField.clear();
        Waiters.waitForAnimationCompleteOnPage(50);
        Waiters.waitForSpinnerDisable();
        firstListLink.waitForClickable();
        firstListLink.waitForAnimationComplete();
        boolean result = false;
        long count = piiList.stream().filter(o -> o.getAttribute("innerText").equals(piiType.getName())).count();
        if (count > 0) {
            result = true;
        }
        return result;
    }


}
