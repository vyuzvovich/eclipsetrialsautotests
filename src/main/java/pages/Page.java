package pages;

public enum Page {
    LOGIN_PAGE("#!/loginWithDefaultUser", "Login", LoginPage.class),
    HOME_PAGE("#!/status", "Home", HomePage.class),
    LOCATE_PAGE("#!/locate", "Locate", LocatePage.class),
    UPLOAD_CSV_PAGE("#!/locate/files", "UploadCSV", UploadCSVPage.class),
    ADD_TO_CLASSIFICATION_PAGE("#!/locate/tables", "AddClassification", AddToClassificationPage.class),
    RECONCILE_PROJECTS_PAGE("#!/locate/compare", "ReconcileProjects", AddToClassificationPage.class),
    ADMIN_PANEL_PAGE("#!/admin/users", "Admin Panel", AdminPanelPage.class),
    USER_MANAGEMENT_PAGE("#!/admin/users", "User Management", UserManagementPage.class),
    GROUP_MANAGEMENT_PAGE("#!/admin/groups", "Group Management", GroupManagementPage.class),
    PII_TYPE_SETTINGS("#!/admin/piis", "PII Type Settings", PiiManagementPage.class),
    DATA_MODEL_PAGE("#!/model/datamodel", "Data Model", DataModelPage.class),
    CLASSIFICATION_PAGE("#!/model/classify", "Classification", ClassificationPage.class),
    DATA_GROUPS_PAGE("#!/model/groups", "Data Groups", DataGroupsPage.class),
    CONTEXT_PAGE("#!/context", "Context", ContextPage.class),
    RISK_OVERVIEW_PAGE("#!/measurementResults/riskOverview", "Results", RiskOverviewPage.class),
    RISK_DETAILS_PAGE("#!/measurementResults/riskDetails", "Risk Details", RiskDetailsPage.class),
    DIRECT_IDENTIFIERS_PAGE("#!/deidentify/direct", "Direct Identifiers", DirectIdentifiersPage.class),
    QUASI_IDENTIFIERS_PAGE("#!/deidentify/quasi/risk", "Quasi-Identifiers", QuasiIdentifiersPage.class),
    DE_IDENTITY_PAGE("#!/deidentify/destination", "De-Identify", DeIdentityPage.class);

    private String link;
    private String name;
    private Class pageObject;

    Page(String link, String name, Class clazz) {
        this.link = link;
        this.name = name;
        this.pageObject = clazz;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public Class getPageObject() {
        return pageObject;
    }
}
