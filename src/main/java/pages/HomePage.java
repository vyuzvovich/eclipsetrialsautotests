package pages;

import businessobjects.csvmapping.CSVFile;
import core.htmlelementswrapper.Button;
import core.logger.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import pages.blocks.LogsModalWindow;
import pages.blocks.ModalWindow;
import pages.blocks.NewProjectPopUp;
import pages.blocks.StatusTable;
import waiters.Waiters;

import java.util.List;

public class HomePage extends ContentPage {

    private NewProjectPopUp newProjectPopUp;
    private final String STRICTRISK = "Measured Strict L1 ";
    private final String PUBLICRISK = "Measured Public ";
    private final String ACQUAINTANCERISK = "Measured Acquaintance ";
    private final String TRAITIONALRISK = "Measured Traditional ";

    private ModalWindow setAsTemplateModalDialog;

    private LogsModalWindow logsModalWindow;
    private ModalWindow modalWindow;

    private StatusTable statusTable;

    @FindBy(xpath = "//button[@ng-click='vm.StartProject()']")
    private Button newProject;

    @Override
    public Page getPage() {
        return Page.HOME_PAGE;
    }

    public LocatePage createNewProject(String projectName) {
        newProject.click();
        Waiters.waitForVisible(newProjectPopUp);
        newProjectPopUp.createNewProject(projectName);
        Waiters.waitForSpinnerDisable();
        LocatePage locatePage = new LocatePage();
        locatePage.checkPageIsLoaded();
        return locatePage;
    }

    public boolean verifyProjectExists(String projectName) {
        try {
            statusTable.waitForDisplayed();
            statusTable.waitForAnimationComplete();
            return statusTable.containsProjectName(projectName);
        } catch (NoSuchElementException noSuchElementException) {
            return false;
        }
    }

    public void clickOnProjectLink(String projectName) {
        statusTable.clickProjectLink(projectName);
    }

    public void deleteProject(String projectName) {
        statusTable.clickDeleteProject(projectName);
        modalWindow.waitForDisplayed();
        modalWindow.clickOnYes();
        modalWindow.waitForAnimationComplete();
    }

    public HomePage waitForRiskMeasurementCompleted(String projectName) {
        statusTable.isRiskMeasurementFinished(projectName);
        return this;
    }

    public HomePage waitForDeidentificationCompleted(String projectName) {
        statusTable.isDeidentificationFinished(projectName);
        return this;
    }

    public boolean isRiskMeasurementSuccessfull(String projectName) {
        return statusTable.isRiskMeasurementFinished(projectName);
    }

    public boolean isDeIdentificationSuccessfull(String projectName) {
        return statusTable.isDeidentificationFinished(projectName);
    }

    public RiskOverviewPage navigateToRiskOverviewPageForRMProject(String projectName) {
        return statusTable.openRiskMeasurementResults(projectName);
    }

    public RiskOverviewPage navigateToRiskOverviewPageForDeIdentifiedProject(String projectName) {
        RiskOverviewPage riskOverviewPage =  statusTable.openDeIdentificationResults(projectName);
        Waiters.waitForURLContains(Page.RISK_OVERVIEW_PAGE.getLink(), 60);
        return riskOverviewPage;
    }

    public RiskOverviewPage navigateToRiskOverviewPage(String projectName) {
        statusTable.clickProjectLink(projectName);
        RiskOverviewPage riskOverviewPage = new RiskOverviewPage();
        Waiters.waitForURLContains(Page.RISK_OVERVIEW_PAGE.getLink(), 60);
        return riskOverviewPage;
    }

    public CSVFile[] downloadCSVResults(String projectName) {
        return statusTable.getDeidentificationResults(projectName);
    }

    public HomePage cloneProject(String projectNameInit, String projectNameCloned) {
        statusTable.clickCloneProject(projectNameInit);
        Waiters.waitForVisible(newProjectPopUp);
        newProjectPopUp.cloneProject(projectNameCloned);
        return new HomePage();
    }

    public HomePage setAsTemplate(String projectName) {
        statusTable.clickSetAsTemplateProject(projectName);
        //Waiters.waitForVisible(setAsTemplateModalDialog);
        setAsTemplateModalDialog.clickOnYes();
        setAsTemplateModalDialog.waitForAnimationComplete();
        return new HomePage();
    }

    public boolean isLogsModalWindowOpened(String projectName) {
        boolean result = false;
        try {
            if (logsModalWindow.isDisplayed() && logsModalWindow.getTitle().contains(projectName)) {
                result = true;
            }
        } catch (Exception ex) {
            Logger.info("Modal window doesn't exist: " + ex.getMessage());
        }
        return result;
    }

    public void openLogsModalWindow(String projectName) {
        statusTable.openLogsModalWindow(projectName);
        Waiters.waitForElementToDisplay(logsModalWindow, 30);
    }

    public void closeLogsModalWindow() {
        this.logsModalWindow.close();
    }

    public String getProjectLogsModalWindowTitle() {
        return logsModalWindow.getTitle();
    }

    public List<String> getProjectLogsModalWindowTextRows() {
        //TODO: clarify min size, remove magic number 19
        int size = logsModalWindow.getTextRows().size();
        while (size<19){
            Waiters.threadSleep(1);
            size = logsModalWindow.getTextRows().size();
        }
        return logsModalWindow.getTextRows();
    }

    public Double getMeasuredRisk(List<String> rowsList) {
        String line = rowsList.stream().filter(row -> row.contains(STRICTRISK)).findFirst().orElse(null);
        return Double.parseDouble(line.substring(line.indexOf("=") + 1));
    }

    public Double getStrictRisk(List<String> rowsList) {
        String line = rowsList.stream().filter(row -> row.contains(STRICTRISK)).findFirst().orElse(null);
        return Double.parseDouble(line.substring(line.indexOf("=") + 1));
    }

    public Double getPublicRisk(List<String> rowsList) {
        String line = rowsList.stream().filter(row -> row.contains(PUBLICRISK)).findFirst().orElse(null);
        return Double.parseDouble(line.substring(line.indexOf("=") + 1));
    }

    public Double getAcquaintanceRisk(List<String> rowsList) {
        String line = rowsList.stream().filter(row -> row.contains(ACQUAINTANCERISK)).findFirst().orElse(null);
        return Double.parseDouble(line.substring(line.indexOf("=") + 1));
    }

    public Double getTraditionalRisk(List<String> rowsList) {
        String line = rowsList.stream().filter(row -> row.contains(TRAITIONALRISK)).findFirst().orElse(null);
        return Double.parseDouble(line.substring(line.indexOf("=") + 1));
    }

    public boolean isProjectMarkedAsATemplate(String projectName) {
        return statusTable.isProjectMarkedAsATemplate(projectName);
    }



}
