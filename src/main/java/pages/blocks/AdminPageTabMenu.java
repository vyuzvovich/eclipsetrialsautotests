package pages.blocks;

import core.htmlelementswrapper.Link;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;

import java.util.List;

@Block(@FindBy(xpath = "//ul[@class='button-group']"))
public class AdminPageTabMenu extends TopMenu {

    @FindBy(xpath = ".//li[@class = 'subnavlink ng-scope']/a" )
    public List<Link> tabs;

    @Override
    public List<Link> getMenuLinks(){
        return tabs;
    }

}
