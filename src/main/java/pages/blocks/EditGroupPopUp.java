package pages.blocks;

import businessobjects.User;
import businessobjects.enums.Users;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.DropDown;
import core.htmlelementswrapper.Element;
import org.openqa.selenium.support.FindBy;
import pages.GroupManagementPage;
import ru.yandex.qatools.htmlelements.annotations.Block;

@Block(@FindBy(xpath = "//form[@name = 'vm.groupForm']"))
public class EditGroupPopUp extends Element {

    @FindBy(xpath = ".//select[@ng-model='vm.AddUser.user']")
    private DropDown addUserToGroup;

    @FindBy(xpath = ".//select[@ng-model='vm.AddUser.group']")
    private DropDown groups;

    @FindBy(xpath = ".//button[@ng-click = 'addUserForm.$valid && vm.AddUser(vm.AddUser)']")
    private Button addUser;

    @FindBy(xpath = ".//button[@ng-click = 'vm.EditGroup()']")
    private Button saveGroup;

    public GroupManagementPage addUserToGroup(User user) {
        addUserToGroup.selectByVisibleText(user.getUserName());
        groups.selectByVisibleText(user.getLevel().getName());
        addUser.click();
        saveGroup.click();
        return new GroupManagementPage();
    }

    public GroupManagementPage addUserToGroupWithLevel(Users user, Users.UserLevel level) {
        addUserToGroup.selectByVisibleText(user.getName());
        groups.selectByVisibleText(level.getLevel());
        addUser.click();
        saveGroup.click();
        return new GroupManagementPage();
    }
}
