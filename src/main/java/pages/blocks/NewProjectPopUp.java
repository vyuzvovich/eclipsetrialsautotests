package pages.blocks;

import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.TextInput;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;


@Block(@FindBy(xpath = "//div[@class = 'reveal-modal fade medium in']"))
public class NewProjectPopUp extends Element {

    @FindBy(name = "projectName")
    private TextInput projectName;

    @FindBy(xpath = "//button[@ng-click='vm.cancel()']")
    private Button cancel;

    @FindBy(xpath = "//button[@ng-click='vm.createProject()']")
    private Button create;

    @FindBy(xpath = "//button[@ng-click='vm.cloneProject()']")
    private Button clone;

    public void createNewProject(String name){
        projectName.type(name);
        create.waitForClickable();
        create.click();
        this.waitForAnimationComplete();
    }

    public void cloneProject(String name){
        projectName.type(name);
        clone.waitForClickable();
        clone.click();
        clone.waitForNotDisplayed();
        this.waitForAnimationComplete();
    }

}
