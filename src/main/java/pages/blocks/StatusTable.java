package pages.blocks;

import businessobjects.csvmapping.CSVFile;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.HomePage;
import pages.RiskOverviewPage;
import ru.yandex.qatools.htmlelements.annotations.Block;
import waiters.Waiters;

import java.util.List;

@Block(@FindBy(xpath= "//table[@id='statusTable']"))
public class StatusTable extends Element {

    private List<ProjectRow> rows;

    public boolean containsProjectName(String projectName){
        Logger.info("Verify Status table contains project " + projectName);
        Waiters.waitForListOfElementsToDisplay(rows, 10);
        return rows.stream().map(r -> r.getProject().getText()).anyMatch(projectName::equals);
    }

    public void clickDeleteProject(String projectName){
        Logger.info("Click on delete project " + projectName);
        getRowByName(projectName).clickDeleteOption();
    }

    public boolean isRiskMeasurementFinished(String projectName){
        Waiters.waitForSpinnerDisable();
        Waiters.waitForAnimationCompleteOnPage(30);
        Logger.info("Verify risk measurement is finished for project: " + projectName);
        return  getRowByName(projectName).getRiskMeasurementResults().isProgressBarCompleted();
    }

    public boolean isDeidentificationFinished(String projectName){
        Logger.info("Verify de-identification is finished for project: " + projectName);
        return  getRowByName(projectName).getDeIdentificationResults().isProgressBarCompleted();
    }

    private ProjectRow getRowByName(String projectName) {
        Waiters.waitForURLContains(HomePage.getCurrentUrl(), 30);
        this.waitForDisplayed();
        return rows.stream().filter(r -> r.getProject().getText().equals(projectName)).findFirst().get();
    }

    public RiskOverviewPage openRiskMeasurementResults(String projectName) {
        return getRowByName(projectName).getRiskMeasurementResults().navigateToRiskOverviewPage();
    }

    public RiskOverviewPage openDeIdentificationResults(String projectName) {
        return getRowByName(projectName).getDeIdentificationResults().navigateToRiskOverviewPage();
    }

    public CSVFile[] getDeidentificationResults(String projectName) {
        return getRowByName(projectName).getDeIdentificationResults().download().getCSVFromZip();
    }
    public void clickCloneProject(String projectName){
        Logger.info("Click on 'Clone to' project " + projectName);
        ProjectRow row = rows.stream().filter(r -> r.getProject().getText().equals(projectName)).findFirst().get();
        row.clickCloneToOption();
    }

    public void clickSetAsTemplateProject(String projectName) {
        Logger.info("Click on 'Set As Template' for project " + projectName);
        ProjectRow row = rows.stream().filter(r -> r.getProject().getText().equals(projectName)).findFirst().get();
        row.clickSetTemplateOption();
    }

    public void clickProjectLink(String projectName) {
        Logger.info("Click on name for project " + projectName);
        ProjectRow row = rows.stream().filter(r -> r.getProject().getText().equals(projectName)).findFirst().get();
        row.click();
    }

    public void openLogsModalWindow(String projectName){
        Logger.info("Click on 'Logs' for project " + projectName);
        this.getRowByName(projectName).getRiskMeasurementResults().openLogsModalWindow();
    }

    public boolean isProjectMarkedAsATemplate(String projectName){
        Waiters.waitForListOfElementsToDisplay(rows, 20);
        ProjectRow row =  rows.stream().filter(r-> r.getProject().getText().equals(projectName)).findFirst().get();
        return row.isProjectMarkedAsTemplate(projectName);
    }

}
