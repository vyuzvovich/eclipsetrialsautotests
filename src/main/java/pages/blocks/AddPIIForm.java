package pages.blocks;

import businessobjects.PIIType;
import businessobjects.enums.DataTypes;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.CheckBox;
import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.TextInput;
import org.openqa.selenium.support.FindBy;
import pages.PiiManagementPage;
import ru.yandex.qatools.htmlelements.annotations.Block;

import java.util.Arrays;
import java.util.List;

@Block(@FindBy(xpath = "//form[@name='piiForm']"))
public class AddPIIForm extends Element {

    @FindBy(xpath = ".//input[@id='prettyName']")
    private TextInput piiName;

    @FindBy(xpath = ".//input[@type='checkbox' and @ng-model='vm.PIIType.canBeUsedAsID']")
    private CheckBox canBeUsedAsID;

    @FindBy(xpath = ".//input[@type='checkbox' and @value='STRING']")
    private CheckBox chbString;
    @FindBy(xpath = ".//input[@type='checkbox' and @value='LONG']")
    private CheckBox chbLong;
    @FindBy(xpath = ".//input[@type='checkbox' and @value='DOUBLE']")
    private CheckBox chbDouble;
    @FindBy(xpath = ".//input[@type='checkbox' and @value='TIMESTAMP']")
    private CheckBox chbDate;


    @FindBy(xpath = ".//button[@ng-click='vm.validateSave(vm.PIIType, piiForm)']")
    private Button saveBtn;

    //TODO: update after scope claricifation, not fully ready

    /**
     * Method fills Pii Type creation form and clicks Save btn
     * //TODO: need to be updated with new controls filling, not ready
     *
     * @param piiType {@link PIIType}
     * @return {@link PiiManagementPage}
     */
    public PiiManagementPage addPiiType(PIIType piiType) {
        piiName.type(piiType.getName());
        List<DataTypes> checkboxesList = Arrays.asList(piiType.getDataTypes());
        selectDataTypeCheckboxes(checkboxesList);
        saveBtn.click();
        return new PiiManagementPage();
    }


    /**
     * Method fills Pii Type creation form and clicks Save btn
     * //TODO: need to be updated with new controls filling, not ready
     *
     * @param piiType {@link PIIType}
     */
    public void editPiiType(PIIType piiType) {
        piiName.clear();
        piiName.type(piiType.getName());
        deselectAllDataTypeCheckboxes();
        List<DataTypes> checkboxesList = Arrays.asList(piiType.getDataTypes());
        selectDataTypeCheckboxes(checkboxesList);
        saveBtn.click();

    }

    private void deselectAllDataTypeCheckboxes() {
        chbString.deselect();
        chbLong.deselect();
        chbDouble.deselect();
        chbDate.deselect();
    }

    private void selectDataTypeCheckboxes(List<DataTypes> checkboxesList) {
        for (DataTypes chbx : checkboxesList) {
            switch (chbx) {
                case STRING:
                    chbString.select();
                    break;
                case LONG:
                    chbLong.select();
                    break;
                case DOUBLE:
                    chbDouble.select();
                    break;
                case DATE:
                    chbDate.select();
                    break;
            }
        }
    }
}
