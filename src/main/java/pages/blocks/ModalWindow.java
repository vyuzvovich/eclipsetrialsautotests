package pages.blocks;

import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.Element;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;
import java.util.stream.Collectors;

@Block(@FindBy(xpath = "//div[@ng-class='{in: animate}']"))
public class ModalWindow extends Element {

    @FindBy(xpath = "..//button[@ng-click='vm.Yes()']")
    private Button yes;

    @FindBy(xpath = "..//button[@ng-click='vm.No()']")
    private Button no;


    public void clickOnYes(){
        yes.click();
    }

    public void clickOnNo(){
        no.click();
    }

}
