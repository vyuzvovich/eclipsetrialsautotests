package pages.blocks;

import businessobjects.ClassificationTable;
import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.TextInput;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import waiters.Waiters;

import java.util.List;
import java.util.stream.Collectors;

@Block(@FindBy(tagName = "fieldset"))
public class ThreeSetTable extends Element{

    @FindBy(xpath = ".//input[@ng-model = 'vm.tableSearch']")
    private TextInput searchInput;

    @FindBy(xpath = ".//li[@ng-class='headClass(node)' and not(ancestor::li)]/div")
    private List<Element> parentNode;

    @FindBy(xpath = ".//li[@ng-class='headClass(node)']//li")
    private List<Element> childNode;

    public boolean containsTables(List<ClassificationTable> tables, boolean withExtension) {
        Logger.info("Verify Available tables contain: "+tables);
        Waiters.waitForListOfElementsToDisplay(parentNode, 10);
        return parentNode.stream().map(HtmlElement::getText).collect(Collectors.toList())
                .containsAll(tables.stream().map(t -> (withExtension) ? t.getName() : t.getNameWithoutExtension())
                        .collect(Collectors.toList()));
    }

    public boolean containsTable(ClassificationTable table, boolean withExtension) {
        Logger.info("Verify Available tables contain: "+table);
        return parentNode.stream().map(HtmlElement::getText).collect(Collectors.toList()).
                contains((withExtension)? table.getName() : table.getNameWithoutExtension());
    }

    public void selectTable(ClassificationTable table){
        Logger.info("Select table "+table);
        Waiters.waitForListOfElementsToDisplay(parentNode, 5);
        parentNode.stream().filter(t -> t.getText().equals(table.getName())).findFirst().get().click();
    }

    public void selectTableByName(String tableName){
        Logger.info("Select table "+tableName);
        Waiters.waitForListOfElementsToDisplay(parentNode, 5);
        parentNode.stream().filter(t -> t.getText().equals(tableName)).findFirst().get().click();
    }

    public boolean verifyTableColumns(ClassificationTable table){
        List<String> tableColumns = table.getTableColumns();
        Logger.info("Verify table columns are: " + tableColumns);
        Waiters.waitForListOfElementsToDisplay(childNode, 10);
        return childNode.stream().map(HtmlElement::getText).collect(Collectors.toList()).containsAll(tableColumns)
                && (childNode.size() == tableColumns.size());
    }





}
