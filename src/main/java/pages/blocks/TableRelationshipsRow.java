package pages.blocks;

import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import waiters.Waiters;

import java.util.List;

@Block (@FindBy(xpath = "//tr[@ng-init='tableIndex = $index']"))
public class TableRelationshipsRow extends Element {

    @FindBy(xpath = ".//button[@ng-click='vm.newJoin($index)']")
    private Button plusButton;

    @FindBy(xpath = "..//div[@name = 'referringTable']//input")
    private Element childTableInput;

    @FindBy(xpath = "..//span[contains(@ng-bind-html, 'item')]")
    private List<Element> childTables;

    @FindBy(xpath = "..//div[@id = 'uiselect-child']//input")
    private Element childKeyInput;

    @FindBy(xpath = "..//span[contains(@ng-bind-html, 'columnChild')]")
    private List<Element> childKeys;

    @FindBy(xpath = "..//div[@id = 'uiselect-parent']//input")
    private Element parentKeyInput;

    @FindBy(xpath = "..//span[contains(@ng-bind-html, 'columnParent')]")
    private List<Element> parentKeys;

    @FindBy(xpath = ".//td[@class = 'name bot-border']/p")
    private Element parentTableTitle;

    @FindBy(xpath = "..//button/i[@class = ' fa fa-check']")
    private Button confirmModel;

    @FindBy(xpath = "..//button/i[@class = 'fa fa-remove']")
    private Button deleteModel;


    public void addTableRelationShip(String childTableTitle, String childKey, String parentKey){
        Logger.info("Add relationship to table: "+parentTableTitle.getText());
        plusButton.click();
        selectOption(childTableTitle, childTableInput, childTables);
        childKeyInput.waitForClickable();
        selectOption(childKey, childKeyInput, childKeys);
        childKeyInput.waitForAnimationComplete(); //remove after performance is fixed
        parentKeyInput.waitForAnimationComplete(); //remove after performance is fixed
        parentKeyInput.waitForClickable(); //remove after performance is fixed
        selectOption(parentKey, parentKeyInput, parentKeys);
        parentKeyInput.waitForAnimationComplete(); //remove after performance is fixed
        Waiters.waitForSpinnerDisable();
        confirmModel.click();
        Waiters.waitForSpinnerDisable();
    }

    private void selectOption(String optionName, Element inputElement, List<Element> options ){
        Logger.info("Select option: "+optionName);
        inputElement.waitForDisplayed();
        inputElement.click();
        Waiters.waitForListOfElementsToDisplay(options, 10);
        options.stream().filter(c -> c.getText().equals(optionName)).findFirst().get().click();
    }

    public Element getParentTableTitle() {
        return parentTableTitle;
    }
}
