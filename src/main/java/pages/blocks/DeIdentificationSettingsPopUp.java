package pages.blocks;

import businessobjects.dategeneralization.Term;
import businessobjects.enums.QuasiIdentifiers;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.DropDown;
import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.TextInput;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;


@Block(@FindBy(xpath = "//div[@ng-class='{in: animate}']/div"))
public class DeIdentificationSettingsPopUp extends Element {

    @FindBy(xpath = ".//select[@ng-model = 'vm.transformationType']")
    private DropDown maskingOptions;

    @FindBy(name = "startingYear")
    private TextInput startingFrom;

    @FindBy(name = "interval")
    private TextInput inIntervalsOf;

    @FindBy(name = "component")
    private DropDown intervalType;


    @FindBy(xpath = ".//button[@ng-click = 'vm.Ok()']")
    private Button ok;

    public void fillInMaskingSettings(QuasiIdentifiers masking) {
        maskingOptions.selectByVisibleText(masking.toString());
        if(masking.getGeneralization()!=null){
            Term[] terms = masking.getGeneralization().getTerms();
            startingFrom.type(String.valueOf(terms[0].getStartingFrom()));
            inIntervalsOf.type(String.valueOf(terms[0].getInIntervalsOf()));
            intervalType.selectByVisibleText(terms[0].getIntervalType().getValue());
        }
        ok.click();
    }
}
