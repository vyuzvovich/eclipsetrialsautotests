package pages.blocks;

import businessobjects.enums.Users;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.TextInput;
import org.openqa.selenium.support.FindBy;
import pages.UserManagementPage;
import ru.yandex.qatools.htmlelements.annotations.Block;

@Block(@FindBy(xpath = "//form[@name = 'vm.userForm']"))
public class AddUserPopUp extends Element {

    @FindBy(id = "userName")
    private TextInput username;

    @FindBy(name = "password")
    private TextInput password;

    @FindBy(xpath = ".//input[@ng-model='vm.modalUser.verifyPassword']")
    private TextInput confirmPassword;

    @FindBy(xpath = ".//button[contains(@ng-click, 'vm.userForm.$valid && vm.AddUser(vm.modalUser)')]")
    private Button addBtn;

    public UserManagementPage addUser(Users user) {
        username.type(user.getName());
        password.type(user.getPassword());
        confirmPassword.type(user.getPassword());
        addBtn.click();
        return new UserManagementPage();
    }
}
