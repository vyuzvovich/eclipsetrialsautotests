package pages.blocks;

import core.htmlelementswrapper.Link;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;

import java.util.List;

@Block(@FindBy(xpath = "//ul[@class = 'dropdown']"))
public class UserSubMenu extends TopMenu {

    @FindBy(xpath = ".//li/a[@ng-click = 'item.MenuLink()']" )
    public List<Link> subMenuLinks;

    @Override
    public List<Link> getMenuLinks(){
        return subMenuLinks;
    }

    public UserSubMenu hoverUserSubmenuTitle(){
        getUserSubmenuTitle().hoverOverElement();
        this.waitForAnimationComplete();
        return this;
    }

}
