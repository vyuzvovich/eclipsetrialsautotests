package pages.blocks;

import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.Link;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;

@Block(@FindBy (xpath ="//tbody/tr"))
public class UserGroupRow extends Element {

    @FindBy(xpath = "..//td[1]")
    private Element userGroupTitle;

    @FindBy(xpath = "..//td[2]")
    private Element admins;

    @FindBy(xpath = "..//td[3]")
    private Element members;

    @FindBy(className = "icon")
    private Element enabled;

    @FindBy(xpath = "..//td[@class='action']//a")
    private Link edit;

    private EditGroupPopUp editGroupPopUp;

    public Element getUserGroupTitle() {
        return userGroupTitle;
    }

    public Element getAdminsTitle() {
        return admins;
    }

    public Element getMembersTitle() {
        return members;
    }

     public EditGroupPopUp editGroup(){
        edit.click();
        editGroupPopUp.waitForDisplayed();
        return editGroupPopUp;
    }
}
