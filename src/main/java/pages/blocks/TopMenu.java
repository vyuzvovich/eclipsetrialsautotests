package pages.blocks;

import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.Link;
import core.logger.Logger;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.FindBy;
import pages.ContentPage;
import pages.Page;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.annotations.Name;
import waiters.Waiters;

import java.util.List;
import java.util.NoSuchElementException;

@Name("TopMenu")
@Block(@FindBy(xpath = "//section[@class='top-bar-section']"))
public class TopMenu extends Element{

    @FindBy(xpath = "//a[@title='Profile']")
    private Element userSubmenuTitle;

    @FindBy(xpath = ".//ul[@class = 'center-buttons center-margin']//a[@href]" )
    private List <Link> topMenuLinks;

    public boolean verifyUserSubmenuTitle(String expectedTitle){
        Logger.info("Verify top menu title equals to "+expectedTitle);
        return getUserSubmenuTitle().getText().equals(expectedTitle);
    }

    @SuppressWarnings("unchecked")
    public <T extends ContentPage> T navigateToPage(Page page){
        try {
            Link pageLink = getMenuLinks().stream().filter(link -> link.getText().contains(page.getName())).findFirst().get();
            Logger.info("Navigate to page " + page.getName());
            pageLink.waitForAnimationComplete();
            Waiters.waitForAnimationCompleteOnPage(50);
            pageLink.waitForClickable();
            pageLink.click();
            Waiters.waitForURLContains(page.getLink(), 30);
        }catch (StaleElementReferenceException| NoSuchElementException e){
            Logger.debug("Catch exception "+e.getMessage());
        }
        try {
            return (T) page.getPageObject().newInstance();
        } catch (Exception e) {
            Logger.debug("Exception on navigateToPage method for "+ page.getName());
            e.printStackTrace();
        }
        return null;
    }

    public List<Link> getMenuLinks() {
        return topMenuLinks;
    }

   Element getUserSubmenuTitle() {
        return userSubmenuTitle;
    }
}
