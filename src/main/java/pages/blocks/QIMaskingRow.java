package pages.blocks;

import businessobjects.enums.Masking;
import businessobjects.enums.QuasiIdentifiers;
import core.htmlelementswrapper.Element;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import waiters.Waiters;

@Block(@FindBy(xpath = "//tbody[@full-arr='vm.displayQIRisks']/tr"))
public class QIMaskingRow extends DIMaskingRow{

    @FindBy(xpath = ".//td[contains(@ng-click, 'vm.openDeidDetailsModal')]")
    private Element deIdentification;

    @FindBy(xpath = ".//td[@ng-if = '!qiRow.isGroup']")
    private Element field;

    @FindBy(xpath = ".//td[contains(@ng-click, 'vm.openDeidDetailsModal')]")
    private Element maskingDetails;

    private DeIdentificationSettingsPopUp deIdentificationSettings;

    public QIMaskingRow setMasking(QuasiIdentifiers masking){
        deIdentification.waitForAnimationComplete();
        deIdentification.click();
        deIdentificationSettings.waitForDisplayed();
        deIdentificationSettings.fillInMaskingSettings(masking);
        Waiters.waitForElementToDisplay(deIdentification, 3);
        return this;
    }

    @Override
    public QIMaskingRow setDefaultMasking(){
        businessobjects.PIIType type = getCurrentRowClassification().getPIITypeObject();
        QuasiIdentifiers masking = (type.getMasking()==null) ? (QuasiIdentifiers) type.getDefaultMasking()
                : Masking.QUASI_IDENTIFIERS.valueOf(type.getMasking());
        setMasking(masking);
        return this;
    }

    @Override
    public Element getField() {
        return field;
    }

    @Override
    public Element getMaskingDetails() {
        return maskingDetails;
    }
}
