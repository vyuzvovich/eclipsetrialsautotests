package pages.blocks;

import core.htmlelementswrapper.Element;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;
import java.util.stream.Collectors;

@Block(@FindBy(xpath = "//div[@ng-transclude]"))
public class LogsModalWindow extends Element {

    @FindBy(xpath = "..//h4")
    private Element modalWindowTitle;

    @FindBy(xpath = "..//p[@ng-repeat]")
    private List<Element> textRows;

    @FindBy(xpath="..//a[@ng-click=\"vm.Cancel()\"]")
    private Element close;

    public String getTitle(){
        return modalWindowTitle.getText();
    }

    public List<String> getTextRows(){
        List<String> result = textRows.stream().map(HtmlElement::getText).collect(Collectors.toList());
        return result;
    }

    public void close(){
        close.click();
    }
}
