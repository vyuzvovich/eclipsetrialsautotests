package pages.blocks;

import businessobjects.ClassificationObject;
import businessobjects.ClassificationTable;
import businessobjects.DataModel;
import core.htmlelementswrapper.Element;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import waiters.Waiters;

import java.util.List;

@Block(@FindBy(xpath = "//tbody[@full-arr='vm.columnList']/tr"))
public class ClassificationRow extends Element {

    public static final String UNKNOWN = "Unknown";
    public static final String PLEASE_CLASSIFY = "Please Classify";
    private static List <ClassificationObject> classifications = ClassificationObject.getDefaultClassification();

    @FindBy(xpath = ".//td[@class = 'secondaryContent'][1]")
    protected Element table;

    @FindBy(xpath = ".//td[@class = 'secondaryContent'][2]")
    protected Element field;

    @FindBy(xpath = ".//td[contains(@ng-click, 'COLUMN_SEMANTICTYPE')]")
    private Element PIIType;

    @FindBy(xpath = "//a[@class = 'ui-select-choices-row-inner']")
    private List<Element> options;

    @FindBy(xpath = ".//td[contains(@ng-click, 'COLUMN_CLASSIFICATION')]")
    protected Element classification;

    @FindBy(xpath = ".//td[contains(@ng-click, 'COLUMN_DATATYPE')]")
    private Element datatype;

    @FindBy(xpath = ".//td[contains(@ng-click, 'vm.addComment')]")
    private Element comments;

    @FindBy(xpath = ".//td[contains(@ng-click, 'vm.openOptionalSettings')]")
    private Element settings;

    protected ClassificationObject getCurrentRowClassification(){
        field.waitForDisplayed();
        field.waitForClickable();
        //remove after improved performance
        Waiters.waitForSpinnerDisable();
        field.waitForAnimationComplete();
        return classifications.stream().filter(c -> c.getFieldName().equals(this.getField().getText())).findFirst().get();
    }

    public boolean matchesTable(ClassificationTable classificationTable, DataModel model){
        String[] dataTypeValue = getDatatype().getText().split(" ");
        ClassificationObject classification = getCurrentRowClassification();
        boolean dataTypeMatcher = dataTypeValue.length>1 ? dataTypeValue[0].equals(classification.getDataType()) &&
                dataTypeValue[1].equals(classification.getDateFormat()): datatype.getText().equals(classification.getDataType());
        boolean PIITypeAndClassificationMatcher = model!=null && (field.getText().equals(model.getChildKey()) || field.getText().equals(model.getParentKey()))
                ? PIIType.getText().equals(classification.getPIIType()) && this.classification.getText().equals(classification.getClassification()) :
                PIIType.getText().equals(UNKNOWN)
                && this.classification.getText().equals(PLEASE_CLASSIFY);
        return classificationTable.getNameWithoutExtension().equals(table.getText()) && classificationTable.getTableColumns().stream().anyMatch(t -> t.equals(field.getText()))
                && dataTypeMatcher && PIITypeAndClassificationMatcher;
    }

    public ClassificationRow setDefaultClassification(){
        setOption(classification, getCurrentRowClassification().getClassification());
        return this;
    }

    public ClassificationRow setClassification(String value){
        setOption(classification, value);
        return this;
    }

    public ClassificationRow setDataType(String dataType){
        setOption(datatype, dataType);
        return this;
    }


    public ClassificationRow setDefaultPIIType(){
        PIIType.waitForAnimationComplete(); //TODO remove after performance is fixed
        setOption(PIIType, getCurrentRowClassification().getPIIType());
        return this;
    }

    public ClassificationRow setPIIType(String value){
        setOption(PIIType, value);
        return this;
    }

    void setOption(Element expandElement, String option){
        expandElement.click();
        Waiters.waitForListOfElementsToDisplay(options, 10);
        options.stream().filter(p -> p.getText().equals(option)).findFirst().get().click();
        expandElement.waitForAnimationComplete();
    }

    public Element getPIIType() {
        return PIIType;
    }

    public Element getClassification() {
        return classification;
    }

    public Element getField() {
        return field;
    }

    private Element getDatatype() {
        return datatype;
    }
}
