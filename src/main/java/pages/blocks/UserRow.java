package pages.blocks;

import core.htmlelementswrapper.CheckBox;
import core.htmlelementswrapper.Element;
import core.htmlelementswrapper.Link;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;

@Block(@FindBy(xpath = "//tbody/tr"))
public class UserRow extends Element {

    @FindBy(xpath = ".//td[1]/p")
    private Element username;

    @FindBy(xpath = ".//td[2]")
    private Element enabled;

    @FindBy(xpath = ".//td[3]")
    private Element reset;

    @FindBy(className = "icon")
    private CheckBox enabledCheckbox;

    @FindBy(xpath = ".//td[@class='action']//a")
    private Link resetPassword;

    public boolean containsUsername(String name) {
        if (username.getText().equals(name)) {
            return true;
        }
        return false;
    }

    public boolean rowEnabled(String name) {
        if (username.getText().equals(name) && enabledCheckbox.isSelected()) {
            return true;
        }
        return false;
    }
}
