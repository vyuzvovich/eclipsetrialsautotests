package pages.blocks;

import core.driver.DriverManager;
import core.htmlelementswrapper.ContextDropDown;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;

@Block(@FindBy(xpath = ".//tbody/tr[@ng-repeat-start]"))
public class ProjectRow extends Element {


    @FindBy(xpath = "//following-sibling::tr[@ng-repeat-end][1]")
    private ResultsRow riskMeasurementResults;

    @FindBy(xpath = "//following-sibling::tr[@ng-repeat-end][2]")
    private ResultsRow deIdentificationResults;

    @FindBy(xpath = ".//td[@class = 'name project-pointer']")
    private Element project;

    @FindBy(xpath = ".//td[@class = 'actions']")
    private Element actionsTooltip;

    @FindBy(xpath = "//ul[@id = 'dropdown-example']")
    private ContextDropDown actionsTooltipMenu;

    @FindBy(xpath = "//i[@ng-if=\"project.isTemplate\"]")
    private Element templateIcon;

    Element getProject() {
        return project;
    }
    public boolean isProjectMarkedAsTemplate(String projectName) {
        return templateIcon.isDisplayed();
    }

    void clickDeleteOption() {
        actionsTooltip.click();
        actionsTooltipMenu.waitForDisplayed();
        actionsTooltipMenu.selectByVisibleText(ContextDropDown.TooltipMenuOptions.DELETE);
    }

    public ResultsRow getDeIdentificationResults() {
        return deIdentificationResults;
    }

    ResultsRow getRiskMeasurementResults() {
        return riskMeasurementResults;
    }

    void clickCloneToOption() {
        actionsTooltip.click();
        actionsTooltipMenu.waitForDisplayed();
        actionsTooltipMenu.selectByVisibleText(ContextDropDown.TooltipMenuOptions.CLONE);
    }

    void clickSetTemplateOption() {
        actionsTooltip.click();
        actionsTooltipMenu.waitForDisplayed();
        actionsTooltipMenu.selectByVisibleText(ContextDropDown.TooltipMenuOptions.SET_TEMPLATE);
        templateIcon.waitForDisplayed();
    }

    void clickDownloadProjectSettingsOption() {
        actionsTooltip.click();
        actionsTooltipMenu.waitForDisplayed();
        actionsTooltipMenu.selectByVisibleText(ContextDropDown.TooltipMenuOptions.DOWNLOAD_SETTINGS);
    }

    void clickProjectPermissionsOption() {
        actionsTooltip.click();
        actionsTooltipMenu.waitForDisplayed();
        actionsTooltipMenu.selectByVisibleText(ContextDropDown.TooltipMenuOptions.PERMISSIONS);
    }


}
