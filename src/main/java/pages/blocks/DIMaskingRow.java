package pages.blocks;

import businessobjects.enums.Masking;
import core.htmlelementswrapper.Element;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import waiters.Waiters;

@Block(@FindBy(xpath = "//tbody[@full-arr='vm.DIsDisplayArray']/tr"))
public class DIMaskingRow extends ClassificationRow {

    public static final String PLEASE_SPECIFY = "Please Specify";

    @FindBy(xpath = ".//td[@class = 'secondaryContent'][3]")
    private Element PIIType;

    @FindBy(xpath = ".//td[@class = 'secondaryContent'][4]")
    private Element datatype;

    @FindBy(xpath = ".//td[contains(@ng-click, 'vm.OpenDropdown')]")
    private Element maskingDetails;

    public DIMaskingRow setMasking(Masking.DIRECT_IDENTIFIERS masking){
        setOption(maskingDetails, masking.toString());
        return this;
    }

    public DIMaskingRow setDefaultMasking(){
        businessobjects.PIIType type = getCurrentRowClassification().getPIITypeObject();
        String masking = (type.getMasking()==null) ? type.getDefaultMasking().toString() : type.getMasking();
        Waiters.waitForSpinnerDisable();
        setOption(maskingDetails, masking );
        return this;
    }

    public Element getMaskingDetails() {
        return maskingDetails;
    }
}