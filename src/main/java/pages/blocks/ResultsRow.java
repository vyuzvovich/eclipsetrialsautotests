package pages.blocks;

import businessobjects.csvmapping.CSVFile;
import core.driver.DriverManager;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.RiskOverviewPage;
import ru.yandex.qatools.htmlelements.annotations.Block;
import core.htmlelementswrapper.Link;
import utils.DataUtil;
import utils.ZipFileReader;
import waiters.Waiters;

@Block(@FindBy(xpath = "(//following-sibling::tr[@ng-repeat-end])"))
public class ResultsRow extends Element {

    @FindBy(xpath = ".//i[@tooltip='Download']")
    private Element downloadTooltip;

    @FindBy(xpath = ".//i[@tooltip='Logs']")
    private Element logsIcon;

    @FindBy(className = "meter")
    private Element progressBar;

    @FindBy(xpath = ".//a[@ng-click ='vm.openCompletedJob(project, job)']/span")
    private Link result;

    private String zipFileName;

    boolean isProgressBarCompleted() {
        progressBar.waitForDisplayed();
        progressBar.waitAttributeValuePresent("style", "width: 100%;", 150);
        return progressBar.getAttribute("style").contains("width: 100%;");
    }

    RiskOverviewPage navigateToRiskOverviewPage(){
        Waiters.waitForSpinnerDisable();
        result.click();
        result.waitForNotDisplayed();
        return new RiskOverviewPage();
    }

    ResultsRow download() {
        Logger.info("Click on download tooltip");
        downloadTooltip.click();
        setZipFileName();
        return this;
    }

    void openLogsModalWindow(){
        logsIcon.click();
        Waiters.waitForSpinnerDisable();
    }

    CSVFile[] getCSVFromZip() {
        return new ZipFileReader(DriverManager.MOUNT_DIR + zipFileName).getCSVFilesFromZip();
    }

    private void setZipFileName() {
        String[] resultsName = (result.getText().contains("- transform -")) ? result.getText().split("- transform -") :
                result.getText().split("- Strict average -");
        this.zipFileName = resultsName[0].trim() + "-Project&Results - " + DataUtil.serverDateTime()+".zip";
    }
}
