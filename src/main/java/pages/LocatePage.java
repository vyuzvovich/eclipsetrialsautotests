package pages;

import businessobjects.DataSource;
import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.ContextDropDown;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import waiters.Waiters;

import java.util.NoSuchElementException;

public class LocatePage extends ContentPage {

    @FindBy(xpath = "//a[@dropdown-toggle='#create-new']")
    private Button createNew;

    @FindBy(id = "create-new")
    private ContextDropDown uploadOptions;

    @Override
    public Page getPage() {
        return Page.LOCATE_PAGE;
    }

    @SuppressWarnings("unchecked")
    public <T extends ContentPage> T createDataSource(DataSource dataSource) {
        Waiters.waitForAnimationCompleteOnPage(30);
        Waiters.waitForSpinnerDisable();
        this.checkPageIsLoaded();
        createNew.waitForAnimationComplete();
        selectDatasourceType(dataSource);
        try {
            return (T) dataSource.getType().getGetUploadPageClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void selectDatasourceType(DataSource dataSource) {
        do {
            try {
                createNew.click();
                uploadOptions.waitForDisplayed();
                if (uploadOptions.getAllOptions().size() < 1) {
                    Waiters.waitForAnimationCompleteOnPage(30);
                    uploadOptions.waitForDisplayed();
                }
                uploadOptions.selectByVisibleText(dataSource.getType().getUploadOption().getName());
                return;
            } catch (NoSuchElementException nex) {
                Logger.debug("Cannot expand dropdown list");
            }
        } while (!uploadOptions.isSelected());
    }
}
