package pages;

import businessobjects.DataSource;
import businessobjects.csvmapping.CSVFile;
import core.htmlelementswrapper.*;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import waiters.Waiters;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UploadCSVPage extends ContentPage {

    @FindBy(xpath = "//input[@ng-model='vm.currentDatasource.name']")
    private TextInput name;

    @FindBy(xpath = "//select[@ng-model='vm.currentDatasource.fieldDelimiter']")
    private DropDown delimiter;

    @FindBy(xpath = "//input[@ng-value='true']")
    private RadioButton firstLineInHeaderTrue;

    @FindBy(xpath = "//input[@ng-value='false']")
    private RadioButton firstLineInHeaderFalse;

    @FindBy(xpath = "//select[@ng-model='vm.currentDatasource.escapeCharacter']")
    private DropDown escapeCharacter;

    @FindBy(xpath = "//select[@ng-model='vm.currentDatasource.quoteCharacter")
    private DropDown quoteCharacter;

    @FindBy(xpath = "//button[@ng-click='vm.createDatasource()']")
    private Button saveSource;

    @FindBy(xpath = "//input[@type='file']")
    private FileInput uploadFile;

    @FindBy(xpath = "//table[@class='spacedTable']//td[@class='name']")
    private List<Element> loadedFiles;

    @FindBy(xpath = "//button[@ng-disabled='vm.continue_F()']")
    private Button continueBtn;

    @Override
    public Page getPage() {
        return Page.UPLOAD_CSV_PAGE;
    }


    public void upload(DataSource dataSource) {
        CSVFile[] files = dataSource.getCSVFiles();
        Waiters.waitForSpinnerDisable();
        name.type(dataSource.getName());
        saveSource.click();
        Waiters.waitForSpinnerDisable();
        for (CSVFile file : files) {
            uploadFile.setFileToUpload(file.getFilePath());
            Waiters.waitForSpinnerDisable();
        }
        uploadFile.waitForAnimationComplete();
    }

    public boolean verifyDataSourceUploaded(DataSource dataSource) {
        Logger.info("Verify DataSource is uploaded");
        Waiters.waitForListOfElementsToDisplay(loadedFiles, 20);
        Waiters.waitForSpinnerDisable();
        return loadedFiles.stream().map(HtmlElement::getText).collect(Collectors.toList()).
                containsAll(Arrays.stream(dataSource.getCSVFiles()).map(CSVFile::getName).collect(Collectors.toList()));
    }

    public AddToClassificationPage navigateToAddClassification() {
        continueBtn.click();
        return new AddToClassificationPage();
    }

}
