package pages;

import businessobjects.DataModel;
import businessobjects.DataSource;
import core.htmlelementswrapper.Button;
import org.openqa.selenium.support.FindBy;
import pages.blocks.TableRelationshipsRow;
import waiters.Waiters;

import java.util.List;

public class DataModelPage extends ContentPage {

    @Override
    public Page getPage() {
        return Page.DATA_MODEL_PAGE;
    }

    private List<TableRelationshipsRow> tableRelationshipsRows;

    @FindBy(xpath = "//button[@ng-click = 'vm.confirmDataModel()']")
    private Button continueButton;


    public DataModelPage setTableRelationShips(DataSource dataSource) {
        Waiters.waitForSpinnerDisable();

        this.checkPageIsLoaded();

        Waiters.waitForListOfElementsToDisplay(tableRelationshipsRows, 20);
        for (DataModel model : dataSource.getDataModels()) {
            tableRelationshipsRows.stream().filter(t -> t.getParentTableTitle().getText().equals(model.getParentTable().getNameWithoutExtension()))
                    .findFirst().get().addTableRelationShip(model.getChildTable().getNameWithoutExtension(), model.getChildKey(), model.getParentKey());
        }
        Waiters.waitForSpinnerDisable();
        return this;
    }

    public ClassificationPage navigateToClassificationPage() {
        Waiters.waitForSpinnerDisable();
        Waiters.waitForAnimationCompleteOnPage(30);
        //TODO: special case, remove after performance issue disappeared
        Waiters.threadSleep(10);
        continueButton.waitForClickable();
        continueButton.click();
        Waiters.waitForSpinnerDisable();
        return new ClassificationPage();
    }

}
