package pages;

import core.htmlelementswrapper.Button;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.support.FindBy;
import pages.blocks.LogsModalWindow;
import utils.DataUtil;

public class RiskOverviewPage extends ContentPage {

    @FindBy(xpath = "//h4[@ng-if]")
    private Element compliantTitle;

    @FindBy(xpath = "//button[@ui-sref='measurementResults.details']")
    private Button continueButton;

    @FindBy(xpath = "//a[@dropdown-toggle=\"#download-functions\"]")
    private Element downloadsLink;

    @FindBy(xpath = "//div[@class=\"radial-progress ng-binding\"]")
    private Element percentageCompliant;

    @FindBy(xpath = "//a[@ng-click=\"vm.DownloadReport('application/pdf')\"]")
    private Element downloadReportSummaryPdf;

    @FindBy(xpath = "//a[@ng-click=\"vm.DownloadReport('application/zip')\"]")
    private Element downloadReportComprehensiveZip;

    @FindBy(xpath = "//a[@ng-click=\"vm.Dvm.DownloadManifest()\"]")
    private Element downloadProjectZip;

    private LogsModalWindow riskResultWarningAndErrors;

    @Override
    public Page getPage() {
        return Page.RISK_OVERVIEW_PAGE;
    }

    public boolean isResultCompliant() {
        Logger.info("Verify result is compliant");
        compliantTitle.waitForDisplayed();
        return compliantTitle.getAttribute("ng-if").contains("vm.isCompliant");
    }

    public RiskDetailsPage navigateToRiskDetailsPage() {
        continueButton.click();
        return new RiskDetailsPage();
    }

    public String getPercentageCompliant() {
        return percentageCompliant.getText();
    }

    public String downloadReportSummary(String projectName) {
        downloadsLink.waitForClickable();
        downloadsLink.click();
        downloadReportSummaryPdf.waitForDisplayed();
        Logger.info("Download report summary");
        downloadReportSummaryPdf.click();
        String pdfName = setReportSummaryPDFName(projectName);
        return pdfName;
    }

    private String setReportSummaryPDFName(String projectName) {
        String result = projectName + " - " + DataUtil.serverDateTime() + ".pdf";
        return result;
    }


    public void closeWarningsIfExist() {
        try {
            riskResultWarningAndErrors.close();
        }
        catch (Exception ex){
            Logger.debug("Exception on riskResultWarningAndErrors closing "+ex.getMessage());
        }

    }
}
