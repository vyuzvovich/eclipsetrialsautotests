package waiters;

import core.driver.DriverManager;
import core.htmlelementswrapper.Element;
import core.logger.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.ContentPage;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

public class Waiters<T> {

    public static void waitForPageLoaded(String pageLink, Integer timeOut) {
        String currentUrl = ContentPage.getCurrentUrl();
        try {
            new WebDriverWait(DriverManager.getDriver(), timeOut).until(
                    e -> currentUrl.contains(pageLink));
        }
        catch (Exception e){
            Logger.error("Current url "+currentUrl+" doesn't contain "+pageLink);
        }

    }

    public static void waitForNumberOfWindowsToEqual(final int numberOfWindows, Integer timeOutSec) {
        try {
            new WebDriverWait(DriverManager.getDriver(), timeOutSec) {
            }.until((ExpectedCondition<Boolean>) driver -> (driver.getWindowHandles().size() == numberOfWindows));
        } catch (Exception ex){
            Logger.error(ex.getMessage());
        }
    }


    public static void waitForSpinnerDisable(){
        new WebDriverWait(DriverManager.getDriver(),90).until(ExpectedConditions.invisibilityOfElementLocated(By.className("spinner-show")));
    }

    public static <T extends Element> void waitForListOfElementsToDisplay(List<T> list, int timeout){
        waitForSpinnerDisable();
        while (timeout>0){
            try {
                if (list.get(0).isDisplayed()){
                    return;
                }
            }catch (IndexOutOfBoundsException e){Logger.debug("Catch IndexOutOfBoundsException. Wait for element to be visible: " + timeout + " seconds");
                    threadSleep(1);
                    timeout--;
            }
        }
    }

    public static void threadSleep(int sec){
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void waitForElementToDisplay(Element element, int timeout){
        while (timeout>0){
            if (element.isDisplayed()){
                return;
            }
            element.sleep(1);
            timeout--;
            Logger.debug("Wait for element to be visible: " + timeout + " seconds");
        }
    }

    public static boolean waitForAnimationCompleteOnPage(Integer timeOutSec) {

        try {
            WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), timeOutSec);

            ExpectedCondition<Boolean> jQueryLoad = driver -> {
                try {
                    return ((JavascriptExecutor)DriverManager.getDriver()).executeScript("return jQuery.active").toString().equals("0");
                }
                catch (Exception e) {
                    return true;
                }
            };

            ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor)DriverManager.getDriver()).executeScript("return document.readyState")
                    .toString().equals("complete");

            return wait.until(jQueryLoad) && wait.until(jsLoad);
        } catch (Exception ex){
            return false;
        }
    }

    public static void waitForVisible(HtmlElement element){
        WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(),5);
        wait.until(ExpectedConditions.visibilityOf(element.getWrappedElement()));
    }

    public static void waitForURLContains(String url, int timeout){
        WebDriver driver = DriverManager.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        ExpectedCondition<Boolean> urlIsCorrect = arg0 ->driver.getCurrentUrl().contains(url);
        wait.until(urlIsCorrect);
    }

}
