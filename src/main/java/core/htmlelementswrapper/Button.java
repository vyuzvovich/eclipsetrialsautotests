package core.htmlelementswrapper;

import core.logger.Logger;
import org.openqa.selenium.WebElement;

public class Button extends ru.yandex.qatools.htmlelements.element.Button implements Extendable, Clickable{
    /**
     * Specifies wrapped {@link WebElement}.
     *
     * @param wrappedElement {@code WebElement} to wrap.
     */
    public Button(WebElement wrappedElement) {
        super(wrappedElement);
    }

    /**
     * Clicks the button.
     */
    @Override
    public void click() {
        this.waitForClickable();
        getWrappedElement().click();
        Logger.info("Click on " + this.getName());
    }

}
