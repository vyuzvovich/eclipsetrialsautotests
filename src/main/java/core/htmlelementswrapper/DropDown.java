package core.htmlelementswrapper;

import core.logger.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class DropDown extends ru.yandex.qatools.htmlelements.element.Select implements Extendable {

    /**
     * Specifies wrapped {@link WebElement}.
     * Performs no checks unlike {@link org.openqa.selenium.support.ui.Select}. All checks are made later
     * in {@link #getSelect()} method.
     *
     * @param wrappedElement {@code WebElement} to wrap.
     */
    public DropDown(WebElement wrappedElement) {
        super(wrappedElement);
    }


    /**
     * Constructs instance of {@link org.openqa.selenium.support.ui.Select} class.
     *
     * @return {@link org.openqa.selenium.support.ui.Select} class instance.
     */
    private org.openqa.selenium.support.ui.Select getSelect() {
        return new org.openqa.selenium.support.ui.Select(getWrappedElement());
    }

    /**
     * Select all options that display text matching the argument. That is, when given "Bar" this
     * would select an option like:
     *
     * &lt;option value="foo"&gt;Bar&lt;/option&gt;
     *
     * @param value The visible text to match against
     * @throws NoSuchElementException If no matching option elements are found
     */
    public void selectByVisibleText(String value) {
        getSelect().selectByVisibleText(value);
        Logger.info("Select from " + this.toString() + " by text:" + value);
    }

    /**
     * Select all options that have a value matching the argument. That is, when given "foo" this
     * would select an option like:
     *
     * &lt;option value="foo"&gt;Bar&lt;/option&gt;
     *
     * @param value The value to match against
     * @throws NoSuchElementException If no matching option elements are found
     */
    public void selectByValue(String value){
        getSelect().selectByValue(value);
        Logger.info("Select from " + this.toString() + " by value:" + value);
    }

    public void click(){
        this.getWrappedElement().click();
    }

    public WebElement getFirstSelected() {
        this.waitForClickable();
        DropDown mySelect = new DropDown(getWrappedElement());
        return mySelect.getFirstSelectedOption();
    }
}
