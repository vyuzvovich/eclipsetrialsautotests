package core.htmlelementswrapper;

import core.driver.DriverManager;
import core.logger.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsElement;
import ru.yandex.qatools.htmlelements.element.Named;

public interface Clickable extends WrapsElement, Named, Extendable{

    default void clickUsingAction(){
       Logger.info("Click on element using actions "+this.getName());
       this.waitForClickable();
       new Actions(DriverManager.getDriver()).moveToElement(this.getWrappedElement()).click().perform();
    }

    default void clickWithJs() {
        Logger.info("Click on element using JS "+this.getName());
        this.waitForClickable();
        ((JavascriptExecutor) DriverManager.getDriver()).executeScript("arguments[0].click();", getWrappedElement());
    }

    default void clickUsingKeys(){
        Logger.info("Click on element sending keys "+this.getName());
        this.waitForClickable();
        new Actions(DriverManager.getDriver()).moveToElement(this.getWrappedElement()).sendKeys(Keys.ENTER).perform();
    }

}
