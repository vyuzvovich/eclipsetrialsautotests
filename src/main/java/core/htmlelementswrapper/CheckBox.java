package core.htmlelementswrapper;

import org.openqa.selenium.WebElement;

public class CheckBox extends ru.yandex.qatools.htmlelements.element.CheckBox implements Extendable, Clickable{
    /**
     * Specifies wrapped {@link WebElement}.
     *
     * @param wrappedElement {@code WebElement} to wrap.
     */
    public CheckBox(WebElement wrappedElement) {
        super(wrappedElement);
    }

}
