package core.htmlelementswrapper;

import core.logger.Logger;
import org.openqa.selenium.WebElement;

public class TextInput extends ru.yandex.qatools.htmlelements.element.TextInput implements Extendable{
    /**
     * Specifies wrapped {@link WebElement}.
     *
     * @param wrappedElement {@code WebElement} to wrap.
     */
    public TextInput(WebElement wrappedElement) {
        super(wrappedElement);
    }

    public void type(String text){
        this.sendKeys(text);
        Logger.info("Type into field " + this.getName() + " text: \""+text+"\"");
    }


}
