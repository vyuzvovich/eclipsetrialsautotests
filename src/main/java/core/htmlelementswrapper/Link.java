package core.htmlelementswrapper;

import core.logger.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

public class Link extends ru.yandex.qatools.htmlelements.element.Link implements Extendable, Clickable{
    public Link(WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public void click() {
        Logger.info("Click on link " + this.getText());
        this.waitForClickable();
        getWrappedElement().click();
    }


    public void clickStaleElement(){
        try {
            this.waitForAnimationComplete();
            this.click();
        }catch (StaleElementReferenceException| NoSuchElementException e){
            Logger.info("Catch exception "+e.getMessage());
        }

    }
}
