package core.htmlelementswrapper;

import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ContextDropDown extends Element {

    @FindBy(xpath = "./li")
    private List<Element> options;

    public void selectByVisibleText(String text) {
        options.stream().filter(o -> o.getText().equals(text)).findFirst().get().click();
    }

    public void selectByVisibleText(ContextDropDownOption option) {
        options.stream().filter(o -> o.getText().equals(option.getName())).findFirst().get().click();
    }

    public List<Element> getAllOptions(){
        return options;
    }


    public enum TooltipMenuOptions implements ContextDropDownOption{
        DELETE("Delete"),
        CLONE("Clone to..."),
        SET_TEMPLATE("Set as Template"),
        DOWNLOAD_SETTINGS("Download Project Settings"),
        PERMISSIONS("Project Permissions");

        private String name;

        private TooltipMenuOptions(String name){
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }
    }

    public enum UploadOptions implements ContextDropDownOption{
        UPLOAD_LOCAL("Upload Local"), //For Trials
        //UPLOAD_LOCAL("Upload"), //For Risk
        SAS_SHARED("SAS Shared Drive Folder"),
        DB2("DB2"),
        SHARED_DRIVE("Shared Drive Folder"),
        ORACLE("Oracle"),
        AZURE_SQL("Azure SQL Managed Instance"),
        MS_SQL("Microsoft SQL Server");

        private String name;

        private UploadOptions(String name){
            this.name=name;
        }

        @Override
        public String getName(){
            return name;
        }
    }

}

