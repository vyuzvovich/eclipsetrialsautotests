package core.htmlelementswrapper;

import core.logger.Logger;
import org.openqa.selenium.WebElement;

public class FileInput extends ru.yandex.qatools.htmlelements.element.FileInput implements Extendable{
    /**
     * Specifies wrapped {@link WebElement}.
     *
     * @param wrappedElement {@code WebElement} to wrap.
     */
    public FileInput(WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public void setFileToUpload(final String fileName) {
        Logger.info("Upload the file "+fileName);
        super.setFileToUpload(fileName);
    }


}
