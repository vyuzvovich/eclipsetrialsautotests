package core.htmlelementswrapper;

import core.driver.DriverManager;
import core.logger.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.Named;

public interface Extendable extends WrapsElement, Named{

    default void waitForClickable() {
        try {
            WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 50);
            wait.until(ExpectedConditions.elementToBeClickable(getWrappedElement()));
        }catch (Exception e){
            Logger.error(e.getMessage());
        }
    }

    default void waitForStale(){
        try {
            WebDriverWait wait = new WebDriverWait(DriverManager.getDriver() ,10);
            wait.until(ExpectedConditions.stalenessOf(getWrappedElement()));
        }catch (Exception e){
            Logger.error(e.getMessage());
        }
    }

    default void waitAttributeValuePresent(String attributeName, String attributeValue, int timeOutSec) {
        try {
            new WebDriverWait(DriverManager.getDriver(), timeOutSec).until(e -> getWrappedElement().getAttribute(attributeName).contains(attributeValue));
        } catch (Exception e) {
            Logger.warn("Couldn't wait attribute: " + attributeName + " have value '" + attributeValue + "'");
        }
    }

    default void waitForDisplayed() {
        try {
            WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 50);
            wait.until(ExpectedConditions.visibilityOf(getWrappedElement()));
        } catch (Exception e) {
            Logger.info("Element " + getWrappedElement() + " is not visible");
        }
    }

    default void scrollTo(){
        ((JavascriptExecutor) DriverManager.getDriver()).executeScript("window.scrollTo(0," +
                this.getWrappedElement().getLocation().y + ")");
    }

     default void waitForNotDisplayed(){
         try {
             WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 50);
             wait.until(ExpectedConditions.invisibilityOf(getWrappedElement()));
         }catch (Exception e){
             Logger.error(e.getMessage());
         }
     }

     default void hoverOverElement(){
        this.waitForDisplayed();
         new Actions(DriverManager.getDriver()).moveToElement(this.getWrappedElement()).perform();
     }

    default void waitForAnimationComplete() {
        Point point1;
        Point point2;
        sleep(1);
        try {
            do {
                point1 = getWrappedElement().getLocation();
                sleep(0.5);
                point2 = getWrappedElement().getLocation();
            } while (point1.x != point2.x && point1.y != point2.y);
        } catch (Exception e) {}
    }

    default void sleep(double seconds){
        long millis =(long)(seconds * 1000L);
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
