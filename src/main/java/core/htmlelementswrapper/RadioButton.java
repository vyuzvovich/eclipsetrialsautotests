package core.htmlelementswrapper;

import org.openqa.selenium.WebElement;

public class RadioButton extends ru.yandex.qatools.htmlelements.element.Radio {
    /**
     * Specifies a radio button of a radio button group that will be used to find all other buttons of this group.
     *
     * @param wrappedElement {@code WebElement} representing radio button.
     */
    public RadioButton(WebElement wrappedElement) {
        super(wrappedElement);
    }
}
