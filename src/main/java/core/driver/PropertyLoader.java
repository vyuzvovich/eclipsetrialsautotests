package core.driver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {

    private static Properties driverConfig;
    private static final String DRIVER_CONFIG_PROPERTIES = "driverconfig.properties";

    public enum Property{
        DRIVER_URL("driver.url"),
        BROWSER("browser"),
        APP_URL("app.url");

        private String key;
        private Property(String key){
            this.key=key;
        }
        @Override
        public String toString() {
            return key;
        }
    }

    public static String get(Property property){
        String envVariable = System.getenv(property.key);
        if(driverConfig==null){
            driverConfig = PropertyLoader.loadPropertyByFilePath(DRIVER_CONFIG_PROPERTIES);
        }
        return (envVariable==null) ? driverConfig.getProperty(property.key): envVariable;
    }

    private static Properties loadPropertyByFilePath(String path) {
        Properties properties = new Properties();
           InputStream stream =  PropertyLoader.class.getClassLoader().getResourceAsStream(path);
        try {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}

