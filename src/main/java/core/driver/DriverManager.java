package core.driver;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import core.driver.PropertyLoader.Property;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

public class DriverManager {

    private WebDriver driver;
    private static ThreadLocal<WebDriver> driverPool = new ThreadLocal<>();
    private DesiredCapabilities caps;
    private URL url = null;
    private static final String DOWNLOAD_DIR =(System.getProperty("os.name")).startsWith("Linux") ? "/home/selenium/Downloads/" : System.getProperty("java.io.tmpdir");
    public static final String MOUNT_DIR = (System.getProperty("os.name")).startsWith("Linux") ? "/home/temp/" : System.getProperty("java.io.tmpdir");

    public void setUp(){
        setUrl();
        caps = getCapabilities(PropertyLoader.get(Property.BROWSER));
        driver = new RemoteWebDriver(url, caps);
        setDriver(driver);
        driverPool.get().manage().window().maximize();
    }

    public static WebDriver getDriver(){
        return driverPool.get();
    }

    private void setDriver(WebDriver driver){
        driverPool.set(driver);
    }

    private DesiredCapabilities getCapabilities(String browser){
        DesiredCapabilities capabilities = new DesiredCapabilities();
        switch (browser){
            case BrowserType.CHROME:
                capabilities = DesiredCapabilities.chrome();
                capabilities.setBrowserName("chrome");
                capabilities.setPlatform(Platform.ANY);
                capabilities.setCapability("enableVNC", true);
                ChromeOptions chromeOptions = new ChromeOptions();
                Map<String, Object> chromePreferences = new Hashtable<String, Object>();
                chromePreferences.put("download.default_directory",  DOWNLOAD_DIR);
                chromePreferences.put("download.prompt_for_download", false);
                chromeOptions.setExperimentalOption("prefs", chromePreferences);
                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                break;
            case BrowserType.FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                capabilities.setBrowserName("firefox");
                capabilities.setPlatform(Platform.ANY);
                break;
        }
        return capabilities;
    }

    private void setUrl(){
        if(url==null){
            try {
                url = new URL(PropertyLoader.get(Property.DRIVER_URL));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    public void tearDown(){
            getDriver().quit();
    }

    public void stopDriver(){
        getDriver().close();
    }

}
