package core.logger;

import core.driver.DriverManager;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class ScreenShotUtil {

    private static String screenshotName;

    public static String captureScreenshot() {
        File scrFile = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
        screenshotName = new Date().toString().replace(":", "_").replace(" ", "_") + ".jpg";
        try {
            FileUtils.copyFile(scrFile,
                    new File(System.getProperty("user.dir") + "\\reports\\" + getScreenshotName()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Logger.info("ScreenShot is captured: "+ getScreenshotName());
        return scrFile.getAbsolutePath();
    }

    @Attachment(value = "Take Screenshot of {0}", type = "image/png")
    public static byte[] writeScreenshotToFile() {
        screenshotName = new Date().toString().replace(":", "_").replace(" ", "_") + ".jpg";
        File screenshot =  new File(System.getProperty("user.dir") + "\\reports\\" + getScreenshotName());
        try {
            FileOutputStream screenshotStream = new FileOutputStream(screenshot);
            byte[] bytes = ((TakesScreenshot) DriverManager.getDriver())
                    .getScreenshotAs(OutputType.BYTES);
            screenshotStream.write(bytes);
            screenshotStream.close();
            return bytes;
        } catch (IOException unableToWriteScreenshot) {
            System.err.println("Unable to write "
                    + screenshot.getAbsolutePath());
            unableToWriteScreenshot.printStackTrace();
        }
        return null;
    }

    @Attachment()
    public static byte[] takeScreenShot() {
        return ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    public static String getScreenshotName() {
        return screenshotName;
    }
}
