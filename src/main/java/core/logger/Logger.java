package core.logger;

import io.qameta.allure.Step;

public class Logger{

	/** prefixes for logging operations */
	final String METHOD_STARTED = "METHOD STARTED";
	final String METHOD_PASSED = "METHOD PASSED";
	final String METHOD_FAILED = "METHOD FAILED";
	final String METHOD_SKIPPED = "METHOD SKIPPED";
	final String TEST_STARTED = "TEST STARTED";
	final String TEST_FINISHED = "TEST FINISHED";
	final String TEST_FAILED = "TEST FAILED";
	final String TEST_SUITE_STARTED = "TEST SUITE STARTED";
	final String TEST_SUITE_FINISHED = "TEST SUITE FINISHED";

	private static final String LOGGER_NAME = "trialsLogger";

	/** Logger */
	private static org.apache.log4j.Logger logger= org.apache.log4j.Logger.getLogger(LOGGER_NAME);

	@Step("{0}")
	public static void info(String msg) {
	logger.info(msg);
	}

	public static void info(String msg, Throwable error) {
		logger.info(msg, error);
	}

	public static void info(Object message) {
		logger.info(message);
	}

	public static void debug(String msg) {
		logger.debug(msg);
	}

	public static void debug(Object message) {
		logger.debug(message);
	}

	public static void debug(String msg, Throwable error) {
		logger.debug(msg, error);
	}

	public static void warn(String msg) {
		logger.warn(msg);
	}

	public static void warn(String msg, Throwable error) {
		logger.warn(msg, error);
	}

	public static void warn(Object message) {
		logger.warn(message);
	}

	public static void fatal(String msg) {
		logger.fatal(msg);
	}

	public static void fatal(String msg, Throwable error) {
		logger.fatal(msg, error);
	}

	public static void fatal(Object message) {
		logger.fatal(message);
	}

	public static void error(String msg) {
		logger.error(msg);
	}

	public static void error(String msg, Throwable error) {
		logger.error(msg, error);
	}

	public static void error(Object message) {
		logger.error(message);
	}

	public static void trace(String msg) {
		logger.trace(msg);
	}

	public static void trace(String msg, Throwable error) {
		logger.trace(msg, error);
	}

	public static void trace(Object message) {
		logger.trace(message);
	}
}
