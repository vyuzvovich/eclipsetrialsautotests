package businessobjects;

import java.util.List;

public interface Classifiable {
    String getName();
    Class getRelatedClass();
    List<?>getObjects();
}
