package businessobjects;

import utils.JSONParser;

import java.util.List;

public class ClassificationObject {
    private static List<ClassificationObject> classifications = getDefaultClassification();

    private String fieldName;
    private String PIIType;
    private String classification;
    private String dataType;
    private String [] dateFormat;
    private String javaType;

    public String getFieldName() {
        return fieldName;
    }

    public String getPIIType() {
        return PIIType;
    }

    public PIIType getPIITypeObject(){
        List<PIIType> types = businessobjects.PIIType.getPIITypes();
        return types.stream().filter(t -> t.getName().equals(PIIType)).findFirst().get();
    }

    public String getClassification() {
        return classification;
    }

    public String getDataType() {
        return dataType;
    }

    public String[] getDateFormat() {
        return dateFormat;
    }

    public String getJavaType() {
        return javaType;
    }

    public static List<ClassificationObject> getDefaultClassification() {
        if (classifications == null) {
            return JSONParser.parseToListOfObjects(JSONParser.FileToParse.DEFAULT_CLASSIFICATION);
        }
        return classifications;
    }
/*
    public static List<ClassificationObject> addClassification(ClassificationObject classificationObject) {
        classifications.add(classificationObject);
        return classifications;
    }

    public static List<ClassificationObject> addClassifications(List<ClassificationObject> classificationObjectsList) {
        for (ClassificationObject classificationObject : classificationObjectsList) {
            classifications.add(classificationObject);
        }
        return classifications;
    }*/
}
