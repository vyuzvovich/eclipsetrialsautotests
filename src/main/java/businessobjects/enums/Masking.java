package businessobjects.enums;
import businessobjects.dategeneralization.DateGeneralization;

import java.util.List;

public interface Masking {
    default boolean verifyMaskingForCell(Object cellValue, List<?> result){
            return result.stream().anyMatch(r -> !(r.toString().equals(cellValue.toString())));
        }

    enum DIRECT_IDENTIFIERS implements Masking{
        MASK_AS_ID("Mask as ID using FPE"),
        MASK_AS_NULL("Mask as Null"),
        MASK_RANGE("Mask Range(s)"),
        MASK_CUSTOM("Mask Custom Value(s)"),
        MASK_SHA256("Mask as SHA256"),
        SHUFFLE("Shuffle"),
        CUSTOM("Custom");

        DIRECT_IDENTIFIERS(String value){
            this.value = value;
        }

        String value;

        @Override public String toString(){
            return value;
        }
    }

    class GeneralizationDate implements Masking, QuasiIdentifiers{

        public static final QuasiIdentifiers DEFAULT = new GeneralizationDate().setGeneralization(DateGeneralization.getDefaultGeneralization());

        private String suppressionSettings;
        private String suppressionValue;
        private DateGeneralization generalization;

        public DateGeneralization getGeneralization() {
            return generalization;
        }

        @Override public String toString(){
            return "Generalization";
        }

        public GeneralizationDate setGeneralization(DateGeneralization generalization) {
            this.generalization = generalization;
            return this;
    }

    public boolean verifyGeneralizationForCell(Object date){
        return generalization.getTerms()[0].getGeneralizedDates().contains(date);
    }
}

enum QUASI_IDENTIFIERS implements Masking, QuasiIdentifiers{

    MASKING ("Masking"),
    NONE ("None");

        QUASI_IDENTIFIERS(String value){
            this.value = value;
        }

        public boolean verifyMaskingForCell(Object cellValue, List<?> result){
            switch (value){
                case "None": return true;
            }
            return verifyMaskingForCell(cellValue, result);
        }

        String value;
        private String suppressionSettings;
        private String suppressionValue;

        @Override public String toString(){
            return value;
        }

        public String getSuppressionSettings() {
            return suppressionSettings;
        }

        public void setSuppressionSettings(String suppressionSettings) {
            this.suppressionSettings = suppressionSettings;
        }

        public String getSuppressionValue() {
            return suppressionValue;
        }

        public void setSuppressionValue(String suppressionValue) {
            this.suppressionValue = suppressionValue;
        }

    @Override
    public DateGeneralization getGeneralization() {
        return null;
    }
}
}
