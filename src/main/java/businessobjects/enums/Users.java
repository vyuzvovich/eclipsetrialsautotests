package businessobjects.enums;

import java.util.LinkedHashMap;
import java.util.Map;

public enum Users {

    FIRST_ADMIN("admin", "Admin123"),
    PRIMARY_ADMIN("Primary", "1qaz!QAZ"),
    ADMIN_ADMIN("Admin-Admin", "1qaz!QAZ"),
    ANALYST_ADMIN("Analyst-Admin", "1qaz!QAZ"),
    DATASOURCE_ADMIN("Datasource-Admin", "1qaz!QAZ"),
    ADMIN_MEMBER("Admin-Member", "1qaz!QAZ"),
    ANALYST_MEMBER("Analyst-Member", "1qaz!QAZ"),
    ANALYST_MEMBER_DATASOURCE_MEMBER("Analyst-Member-Datasource-Member", "1qaz!QAZ"),
    DATASOURCE_MEMBER("Datasource-Member", "1qaz!QAZ");


    private String name;
    private String password;
    private Map<UserGroup, UserLevel> groupManagementMap = new LinkedHashMap<>();


    Users(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Map<UserGroup, UserLevel> getGroupManagementMap() {
        return groupManagementMap;
    }

    public void setGroupManagementMap(Map<UserGroup, UserLevel> groupManagementMap) {
        this.groupManagementMap = groupManagementMap;
    }
    

    public Map<UserGroup, UserLevel> assignDefaultGroupManagementMap() {
        switch (this) {
            case FIRST_ADMIN:
                groupManagementMap.put(UserGroup.ANALYSTS, UserLevel.ADMINS);
                groupManagementMap.put(UserGroup.DATASOURCE_ADMINS, UserLevel.ADMINS);
                break;

            case PRIMARY_ADMIN:
                groupManagementMap.put(UserGroup.ADMIN, UserLevel.ADMINS);
                groupManagementMap.put(UserGroup.ANALYSTS, UserLevel.ADMINS);
                groupManagementMap.put(UserGroup.DATASOURCE_ADMINS, UserLevel.ADMINS);
                break;

            case ADMIN_ADMIN:
                groupManagementMap.put(UserGroup.ADMIN, UserLevel.ADMINS);
                break;

            case ANALYST_ADMIN:
                groupManagementMap.put(UserGroup.ANALYSTS, UserLevel.ADMINS);
                break;

            case DATASOURCE_ADMIN:
                groupManagementMap.put(UserGroup.DATASOURCE_ADMINS, UserLevel.ADMINS);
                break;

            case ADMIN_MEMBER:
                groupManagementMap.put(UserGroup.ADMIN, UserLevel.MEMBERS);
                break;

            case ANALYST_MEMBER:
                groupManagementMap.put(UserGroup.ANALYSTS, UserLevel.MEMBERS);
                break;

            case DATASOURCE_MEMBER:
                groupManagementMap.put(UserGroup.DATASOURCE_ADMINS, UserLevel.MEMBERS);
                break;

            case ANALYST_MEMBER_DATASOURCE_MEMBER:
                groupManagementMap.put(UserGroup.ANALYSTS, UserLevel.MEMBERS);
                groupManagementMap.put(UserGroup.DATASOURCE_ADMINS, UserLevel.MEMBERS);
                break;
        }
        return groupManagementMap;
    }

    public enum UserGroup {
        ADMIN("admin"),
        ANALYSTS("analysts"),
        DATASOURCE_ADMINS("DatasourceAdmins"),
        CUSTOM_GROUP("Custom");
        private String name;

        UserGroup(String name) {
            this.name = name;
        }

        public String getGroupName() {
            return name;
        }
    }

    public enum UserLevel {
        ADMINS("Admins"),
        MEMBERS("Members");
        private String level;

        UserLevel(String level) {
            this.level = level;
        }

        public String getLevel() {
            return level;
        }
    }
}

