package businessobjects.enums;

import businessobjects.dategeneralization.DateGeneralization;

public interface QuasiIdentifiers extends Masking{
    DateGeneralization getGeneralization();
}
