package businessobjects.enums;

public enum DataTypes {

    STRING ("String"),
    LONG ("Long"),
    DOUBLE ("Double"),
    DATE("Date");

    private String name;

    DataTypes(String name){
        this.name = name;
    }

    @Override
    public String toString(){
        return name;
    }
}

