package businessobjects.csvmapping;

import businessobjects.Classifiable;
import businessobjects.DataSource;
import core.driver.DriverManager;
import utils.CSVReader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CSVFile implements Classifiable {

    //TODO: move to separate file or to particular test?
    public static final Map<String, String> customFieldsMap = new HashMap<String, String>() {{
        put("MedDRA_LLT", "java.lang.String");
        put("MedDRA_PT", "java.lang.String");
    }};

    public static final CSVFile MEDDRA08AL2 = new CSVFile("csv/MedDRA/MedDRA_08aL2.csv", "MM/dd/yyyy", customFieldsMap);
    public static final CSVFile MEDDRA08AL1 = new CSVFile("csv/MedDRA/MedDRA_08aL1.csv", "MM/dd/yyyy");
    public static final CSVFile MASKINGL1 = new CSVFile("csv/Masking_L1.csv", "MM/dd/yyyy");
    public static final CSVFile MASKINGLU = new CSVFile("csv/Masking_LU.csv", "MM/dd/yyyy");
    public static final CSVFile MEDDRA01L1 = new CSVFile("csv/MedDRA/MedDRA_01L1.csv", "MM/dd/yyyy");
    public static final CSVFile MEDDRA01L2 = new CSVFile("csv/MedDRA/MedDRA_01L2.csv", "MM/dd/yyyy");

    public static final CSVFile DEMOGRAPHICS = new CSVFile("csv/Small_Demographics.csv");
    public static final CSVFile DIAGNOSIS = new CSVFile("csv/Small_Diagnosis.csv");


    public CSVFile(String filePath, String dateFormatToOverwrite, Map<String, String> customFieldsMap) {
        this.filePath = filePath;
        String[] splittedPath = filePath.split("[\\\\, /]");
        this.name = splittedPath[splittedPath.length - 1];
        this.objects = new CSVReader(this, customFieldsMap).parseCSVToObjects(dateFormatToOverwrite);
        this.relatedClass = objects.get(0).getClass();
    }

    public CSVFile(String filePath, String dateFormatToOverwrite) {
        this(filePath, dateFormatToOverwrite, null);
    }

    public CSVFile(String filePath) {
        this(filePath, null, null);
    }

    private String filePath;
    private String name;
    private List<?> objects;
    private Class relatedClass;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class getRelatedClass() {
        return relatedClass;
    }

    public DataSource.DataSourceType getDataSourceType() {
        return DataSource.DataSourceType.CSV;
    }

    public String getFilePath() {
        return (filePath.startsWith(DriverManager.MOUNT_DIR)) ? filePath :
                Objects.requireNonNull(getClass().getClassLoader().getResource(filePath)).getPath();
    }

    public List<?> getObjects() {
        return objects;
    }
}
