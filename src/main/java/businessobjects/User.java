package businessobjects;

import org.apache.commons.lang.ArrayUtils;
import utils.DataUtil;

@Deprecated
public class User {
    private User(String userName, String password){
        this.userName=userName;
        this.password=password;
    }

    private User addUserGroup(UserGroup... group){
        userGroups = (UserGroup[]) ArrayUtils.addAll(userGroups, group);
        return this;
    }

    private String userName;
    private String password;
    private UserGroup[] userGroups;
    private UserLevel level;

    private static User regressionUser;

    public static User getRegressionUser(){
        if(regressionUser == null){
            regressionUser = new User("Primary", "1qaz!QAZ");
            regressionUser.addUserGroup(UserGroup.ANALYSTS).addUserGroup(UserGroup.DATASOURCE_ADMINS);
            regressionUser.setLevel(UserLevel.ADMINS);
        }
        return regressionUser;
    }

    public static User getNewUser(){
        String randomName = DataUtil.randomString(10);
        String randomPassword = DataUtil.randomString(10);
        return new User(randomName,randomPassword);
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public UserGroup[] getUserGroups() {
        return userGroups;
    }

    public UserLevel getLevel() {
        return level;
    }

    public void setLevel(UserLevel level) {
        this.level = level;
    }

    public enum UserGroup {
        ADMIN("admin"), ANALYSTS("analysts"), DATASOURCE_ADMINS("DatasourceAdmins");
        private String name;

        private UserGroup(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public enum UserLevel {
        ADMINS("Admins"),
        MEMBERS("Members");
        private String name;
        private UserLevel(String name){
            this.name = name;
        }
        public String getName() {
            return name;
        }
    }
}
