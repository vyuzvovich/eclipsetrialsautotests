package businessobjects;

public class DataModel {
    public DataModel(ClassificationTable childTable, ClassificationTable parentTable, String childKey, String parentKey){
        this.childTable = childTable;
        this.parentTable = parentTable;
        this.childKey = childKey;
        this.parentKey = parentKey;
    }

    private ClassificationTable childTable;
    private ClassificationTable parentTable;
    private String parentKey;
    private String childKey;

    public ClassificationTable getParentTable() {
        return parentTable;
    }

    public ClassificationTable getChildTable() {
        return childTable;
    }

    public String getParentKey() {
        return parentKey;
    }

    public String getChildKey() {
        return childKey;
    }
}
