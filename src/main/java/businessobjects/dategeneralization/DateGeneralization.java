package businessobjects.dategeneralization;

public class DateGeneralization {

    public DateGeneralization(Term... terms){
        this.terms = terms;
    }

    private Term[] terms;
    boolean groupValuesMoreThan1880;
    boolean groupvaluesLessThan;

    public static DateGeneralization getDefaultGeneralization(){
        return new DateGeneralization(new Term(1880, 1, Term.IntervalType.MONTHS));
    }

    public Term[] getTerms() {
        return terms;
    }
}
