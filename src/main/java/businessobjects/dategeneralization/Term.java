package businessobjects.dategeneralization;

import core.logger.Logger;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Term {
    public Term(int startingFrom, int inIntervalsOf, IntervalType intervalType){
        this.startingFrom = startingFrom;
        this.inIntervalsOf = inIntervalsOf;
        this.intervalType = intervalType;
        this.startDate = getStartDate();
    }

    private int startingFrom;
    private int inIntervalsOf;
    private IntervalType intervalType;
    private LocalDate startDate;
    private List<Date> generalizedDates = new ArrayList<>();

    public LocalDate getStartDate(){
        return LocalDate.of(this.getStartingFrom(), 1, 1);}

    public int getStartingFrom() {
        return startingFrom;
    }

    public int getInIntervalsOf() {
        return inIntervalsOf;
    }

    private List<Date> getListOfIntervals(){
        generalizedDates.add(convertToDate(startDate));
        LocalDate start = startDate;
        while (start.isBefore(LocalDate.now())){
            start = getIntervalType().plusInterval(start, inIntervalsOf);
            generalizedDates.add(convertToDate(start));
        }
        Logger.info("Generalized date's possible values are: " + generalizedDates);
        return generalizedDates;
    }

    private Date convertToDate(LocalDate local){
        return Date.from(local.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public IntervalType getIntervalType() {
        return intervalType;
    }

    public List<Date> getGeneralizedDates() {
        if(generalizedDates.size()==0){
            getListOfIntervals();
        }
        return generalizedDates;
    }

    public enum IntervalType{
        MONTHS("Months"),
        WEEKS("Weeks"),
        DAYS("Days"),
        YEARS("Years");

        private String value;

        IntervalType(String value){
            this.value=value;
        }

        private LocalDate plusInterval(LocalDate date, long number){
            switch (this){
                case MONTHS:return date.plusMonths(number);
                case WEEKS:return date.plusWeeks(number);
                case DAYS:return date.plusDays(number);
                case YEARS:return date.plusYears(number);
                default:return date.plusMonths(number);
            }
        }

        public String getValue() {
            return value;
        }
    }
}
