package businessobjects;

import businessobjects.enums.DataTypes;
import businessobjects.enums.Masking;
import utils.JSONParser;
import java.util.List;

public class PIIType {
    public PIIType(String name, DataTypes[] dataTypes){
        this.name = name;
        this.dataTypes = dataTypes;
    }

    private static List<PIIType> semanticTypes = getPIITypes();

    public static List<PIIType> getPIITypes() {
        if (semanticTypes==null){
            return JSONParser.parseToListOfObjects(JSONParser.FileToParse.SEMANTIC_TYPES);
        }
        return semanticTypes;
    }

    private String name;
    private DataTypes[] dataTypes;
    private String masking;
    private int defaultSuppression;
    private boolean canBeUsedAsID;
    private String defaultClassification;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Masking getDefaultMasking(){
        if (defaultClassification.equals("DI")){
            return (masking ==null) ? Masking.DIRECT_IDENTIFIERS.MASK_AS_ID : Masking.DIRECT_IDENTIFIERS.CUSTOM;
        }
        return (name.contains("Date")) ? Masking.GeneralizationDate.DEFAULT : Masking.QUASI_IDENTIFIERS.NONE;
    }

    public DataTypes[] getDataTypes() {
        return dataTypes;
    }

    public void setDataTypes(DataTypes[] dataTypes) {
        this.dataTypes = dataTypes;
    }

    public String getMasking() {
        return masking;
    }

    public void setMasking(String masking) {
        this.masking = masking;
    }

    public int getDefaultSuppression() {
        return defaultSuppression;
    }

    public void setDefaultSuppression(int defaultSuppression) {
        this.defaultSuppression = defaultSuppression;
    }

    public boolean isCanBeUsedAsID() {
        return canBeUsedAsID;
    }

    public void setCanBeUsedAsID(boolean canBeUsedAsID) {
        this.canBeUsedAsID = canBeUsedAsID;
    }

    public String getDefaultClassification() {
        return defaultClassification;
    }

    public void setDefaultClassification(String defaultClassification) {
        this.defaultClassification = defaultClassification;
    }

}
