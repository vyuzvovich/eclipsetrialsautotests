package businessobjects;

import businessobjects.csvmapping.CSVFile;
import businessobjects.enums.Masking;
import core.htmlelementswrapper.ContextDropDown;
import core.logger.Logger;
import org.apache.commons.lang.ArrayUtils;
import pages.UploadCSVPage;
import utils.DataUtil;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataSource{

    private DataSource(){
        name = "Data Source "+ DataUtil.currentDate();
    }

    private String name;

    private List<DataModel> dataModels = new ArrayList<>();

    private CSVFile[] csvFiles;

    private CSVFile[] deidentificationResults;

    private List<ClassificationTable> classificationTables;

    private DataSourceType type;

    private static List <ClassificationObject> classificationObjects = ClassificationObject.getDefaultClassification();

    private Map<String, Masking> maskingMap = new HashMap<>();

    public String getName() {
        return name;
    }

    public static Builder newDataSource() {
        return new DataSource().new Builder();
    }

    public DataSourceType getType() {
        return type;
    }

    public CSVFile[] getCSVFiles() {
        return csvFiles;
    }

    public List<ClassificationTable> getClassificationTables() {
        return classificationTables;
    }

    private ClassificationTable getTableByName(String name){
        return classificationTables.stream().filter(c -> c.getName().equals(name)).findFirst().get();
    }

    public List<DataModel> getDataModels() {
        return dataModels;
    }

    public DataSource addDeidentificationResults(CSVFile[] deidentificationResults) {
        this.deidentificationResults = deidentificationResults;
        return this;
    }

    public boolean verifyMaskingApplied() {
        boolean result = true;
        for (CSVFile file : csvFiles){
            result&=Arrays.stream(file.getRelatedClass().getDeclaredFields()).allMatch(f -> verifyMaskingForField(f.getName(), file));
        }
        return result;
    }

    private boolean verifyMaskingForField(String field, CSVFile file) {
        List<?> inputColumn = getColumnDataByFieldName(file, field);
        List<?> outputColumn = getColumnDataByFieldName(getRelatedDeidResult(file),field);
        Masking masking =(maskingMap.containsKey(field))? maskingMap.get(field): classificationObjects.stream()
                .filter(c -> c.getFieldName().equals(field)).findFirst().get().getPIITypeObject().getDefaultMasking();
        boolean result =  (masking instanceof Masking.GeneralizationDate) ? outputColumn.stream().allMatch(((Masking.GeneralizationDate) masking)::verifyGeneralizationForCell)
                :inputColumn.stream().allMatch(i -> masking.verifyMaskingForCell(i, outputColumn));
        Logger.info("Verify masking is applied for field "+field+": "+result);
        return result;
    }

    private List<?>getColumnDataByFieldName(Classifiable file, String fieldName){
        Field field = null;
        List<Object> list = new ArrayList<>();
        try {
            field = file.getRelatedClass().getDeclaredField(fieldName);
            for (Object item : file.getObjects()) {
                list.add(field.get(item));
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addMaskingItem(String field, Masking masking) {
        maskingMap.put(field, masking);
    }

    public boolean verifyShuffling(){
        return verifyShuffling("row_num");
    }

    private boolean verifyShuffling(String shufflingField) {
        boolean result = true;
        for (CSVFile file : csvFiles){
            List<?> deidentifiedData = getRelatedDeidResult(file).getObjects();
            try {
                Field field = file.getRelatedClass().getField(shufflingField);
                List<?> originalData = file.getObjects();
                int unshuffledRows = 0;
                for (int i = 0; i<originalData.size(); i++){
                    unshuffledRows+=(field.get(originalData.get(i)).equals(field.get(deidentifiedData.get(i)))) ? 1 : 0;
                }
                double notShuffledRowsPercentage = Double.valueOf(unshuffledRows)/Double.valueOf((originalData.size()));
                Logger.info("Not shuffled rows percentage is: " + notShuffledRowsPercentage);
                result&=notShuffledRowsPercentage <= 0.4;
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
            Logger.info("Verify shuffling between files: " + file.getName() + ", "+ getRelatedDeidResult(file).getName()+ " - " + result);
        }
        return result;
    }


    private CSVFile getRelatedDeidResult(CSVFile file){
        return Arrays.stream(deidentificationResults).filter(d -> d.getName().contains(file.getName().split("\\.")[0])).findFirst().get();
    }

    public class Builder {

        public Builder setDataSourceType(DataSourceType type) {
            DataSource.this.type=type;
            return this;
        }

        public Builder addCSV(CSVFile... files){
            if (DataSource.this.csvFiles !=null){
                DataSource.this.csvFiles = (CSVFile[]) ArrayUtils.addAll(DataSource.this.csvFiles, files);
            }
            else {
                DataSource.this.csvFiles = files;
            }
            setClassificationTables(files);
            return this;
        }

        public DataSource build() {
            return DataSource.this;
        }

        public Builder addDataModel(Classifiable childTable, Classifiable parentTable, String childKey, String parentKey) {
            dataModels.add(new DataModel(getTableByName(childTable.getName()), getTableByName(parentTable.getName()),childKey, parentKey));
            return this;
        }
    }

    private void setClassificationTables(Classifiable[] classifiable){
        if (DataSource.this.classificationTables!=null){
            DataSource.this.classificationTables.addAll(Arrays.stream(classifiable)
                    .map(c -> new ClassificationTable(c.getName(), c.getRelatedClass()))
                    .collect(Collectors.toList()));
        }
        else {
            DataSource.this.classificationTables=Arrays.stream(classifiable)
                    .map(c -> new ClassificationTable(c.getName(), c.getRelatedClass()))
                    .collect(Collectors.toList());
        }
    }

    public enum DataSourceType{

        CSV (UploadCSVPage.class, ContextDropDown.UploadOptions.UPLOAD_LOCAL);

        DataSourceType(Class uploadPageClass, ContextDropDown.UploadOptions option){
            this.option = option;
            this.uploadPageClass = uploadPageClass;
        }

        private ContextDropDown.UploadOptions option;

        private Class uploadPageClass;

        public ContextDropDown.UploadOptions getUploadOption(){
            return option;
        }

        public Class getGetUploadPageClass(){
            return uploadPageClass;
        }
    }
}
