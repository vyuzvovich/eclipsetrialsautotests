package businessobjects;

import utils.DataUtil;

public class Project{

    public Project(String name){
        setName(name);
    }

    private Project(Project projectToCopy){
        this.name = projectToCopy.getName() + "_cloned";
    }

    private String name;

    public static Project randomProject(){
        return new Project("Project " + DataUtil.currentDate());
    }

    public static Project randomProject(String postfix){
        return new Project("Project " + DataUtil.currentDate()+ postfix);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "{ \"name\": \"" + this.name + "\" }";
    }


    /**
     * Creates a copy of current project,
     * @return {@link Project} project copied
     */
    public Project copy(){
        return new Project(this);
    }
}
