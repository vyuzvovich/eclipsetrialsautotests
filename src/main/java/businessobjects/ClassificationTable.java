package businessobjects;

import com.opencsv.bean.CsvBindByName;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClassificationTable {

     ClassificationTable(String name, Class businessObject){
        this.name = name;
        this.businessObject = businessObject;
        nameWithoutExtension = this.name.split("\\.")[0];
        tableColumns = setColumns();
    }

     private String name;
     private Class businessObject;
     private String nameWithoutExtension;
     private List<String> tableColumns;

    public String getName() {
        return name;
    }

    public Class getBusinessObject() {
        return businessObject;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getNameWithoutExtension() {
        return nameWithoutExtension;
    }


    private List<String> setColumnsForDefinedObjects(){
        return Arrays.stream(businessObject.getDeclaredFields()).map(f ->(f.getAnnotation(CsvBindByName.class).
                column().isEmpty())? f.getName() : f.getAnnotation(CsvBindByName.class).column()).collect(Collectors.toList());
    }

    private List<String> setColumns(){
        return Arrays.stream(businessObject.getDeclaredFields()).map(f -> f.getName()).collect(Collectors.toList());
    }

    public List<String> getTableColumns() {
        return tableColumns;
    }
}
