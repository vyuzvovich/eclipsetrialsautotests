package businessutils;

import businessobjects.Project;
import core.logger.Logger;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import pages.HomePage;
import pages.LocatePage;
import pages.Page;
import pages.RiskOverviewPage;
import pages.blocks.LogsModalWindow;
import pages.blocks.ModalWindow;
import waiters.Waiters;

import java.util.List;

/**
 * Wrapping util for actions from Home page
 */
public class HomeProjectManagementUtil {

    /**
     * Creates new project, returns on Home page and verifies, that project is created
     * @param project to create
     */
    public static void createNewProjectOnHomePage(Project project){
        HomePage homePage = new HomePage();
        LocatePage locatePage = homePage.createNewProject(project.getName());
        Waiters.waitForURLContains(LocatePage.getCurrentUrl(), 60);
        //Waiters.waitForPageLoaded(LocatePage.getCurrentUrl(), 120);
        Waiters.waitForAnimationCompleteOnPage(120);
        homePage = locatePage.getMenu().navigateToPage(Page.HOME_PAGE);
        Assert.assertTrue(homePage.verifyProjectExists(project.getName()), "Project '" + project.getName() + "' is not created");
    }

    /**
     * Clicks on specified project's name on Home page
     * @param project {@link Project} to click
     */
    public static void clickOnProject(Project project) {
        HomePage homePage = new HomePage();
        homePage.clickOnProjectLink(project.getName());
    }

    /**
     * Opens Logs modal window from Home page, returns list of text rows
     * @param  projectName name of project
     * @return list of project logs rows
     */
    public static List<String> getLogsFromModalWindow(String projectName){
        HomePage homePage = new HomePage();
        if (!homePage.isLogsModalWindowOpened(projectName)) {
            homePage.openLogsModalWindow(projectName);
        }
        Logger.info("Modal window is open");
        return homePage.getProjectLogsModalWindowTextRows();
    }

    public static void closeLogsModalWindow(String projectName){
        HomePage homePage = new HomePage();
        if (homePage.isLogsModalWindowOpened(projectName)){
            homePage.closeLogsModalWindow();
        }
    }

    /**
     * Gets Strict L1 risk from already risk rows list
     * @param rows list of project logs rows
     * @return parsed Double value from Strict Risk L1
     */
    public static Double getStrictRiskFromModalWindow(List<String> rows){
        return new HomePage().getStrictRisk(rows);
    }


    /**
     * Gets Traditional Risk risk from already risk rows list
     * @param rows List of String rows of logs
     * @return parsed Double value from Traditional Risk
     */
    public static Double getTraditionalRiskFromModalWindow(List<String> rows){
        return new HomePage().getTraditionalRisk(rows);
    }


    /**
     * Gets Acquaintance risk from already risk rows list
     * @param rows list of project logs rows
     * @return parsed Double value from Acquaintance Risk
     */
    public static Double getAcquaintanceRiskFromModalWindow(List<String> rows){
        return new HomePage().getAcquaintanceRisk(rows);
    }


    /**
     * Gets Public risk from already risk rows list
     * @param rows List of strings, presenting rows of logs
     * @return parsed Double value from Public risk
     */
    public static Double getPublicRiskFromModalWindow(List<String> rows){
        return new HomePage().getPublicRisk(rows);
    }

    /**
     * Sets the project as a Template
     * @param projectName name of project
     */
    public static void setProjectAsTemplate(String projectName){
        new HomePage().setAsTemplate(projectName);
    }

    /**
     *
     * @param projectName name of project
     * @return true if template icon is visible
     */
    public static boolean isProjectMarkedAsTemplate(String projectName){
        return new HomePage().isProjectMarkedAsATemplate(projectName);
    }

    /**
     * Method clicks on De-Identification result row to navigate to Risk management
     * @param projectName name of project
     * @return {@link RiskOverviewPage}
     */
    public static RiskOverviewPage navigateToRiskOverviewPageForDeIdentifiedProject(String projectName) {
        return new HomePage().navigateToRiskOverviewPageForDeIdentifiedProject(projectName);
    }

    /**
     * Method clicks on project row to navigate to Risk management for project, that have eigher de-id, or RM completede
     * @param projectName name of project
     * @return {@link RiskOverviewPage}
     */
    public static RiskOverviewPage navigateToRiskOverviewPage(String projectName) {
        return new HomePage().navigateToRiskOverviewPage(projectName);
    }

    public static boolean isRiskMeasurementSuccessfull(String templateName){
        return new HomePage().isRiskMeasurementSuccessfull(templateName);
    }

    public static boolean isDeIdentificationSuccessfull(String templateName){
        return new HomePage().isDeIdentificationSuccessfull(templateName);
    }
}
