package businessutils;

import businessobjects.enums.Users;
import pages.LoginPage;
import waiters.Waiters;

/**
 * Wrapping class for users authentication
 */
public class AuthenticationUtil {

    /**
     * Logins with Default user (admin)
     */
    public static void loginWithDefaultUser() {
        LoginPage page = new LoginPage().openPage();
        page.loginWithDefaultUser();
        Waiters.waitForAnimationCompleteOnPage(10);
    }

    /**
     * Logins with specified user
     * @param user {@link Users} enum
     */
    public static void loginWithSpecifiedUser(Users user) {
        LoginPage page = new LoginPage().openPage();
        page.loginWithUser(user);
        Waiters.waitForAnimationCompleteOnPage(120);
    }
}
