package businessutils;

import businessobjects.PIIType;
import core.logger.Logger;
import org.testng.Assert;
import pages.HomePage;
import pages.Page;
import pages.PiiManagementPage;
import pages.UserManagementPage;
import waiters.Waiters;

/**
 * Wrapping util for actions on Admin Panel - PII Type Settings
 */
public class PIIAdministrationUtil {

    /**
     * Creates custom Pii Type
     *
     * @param piiType {@link PIIType}
     */
    public static void createPiiCustomType(PIIType piiType) {
        new HomePage().getUserSubMenu().navigateToPage(Page.ADMIN_PANEL_PAGE);
        UserManagementPage userManagementPage = new UserManagementPage();
        PiiManagementPage piiManagementPage = userManagementPage.getAdminPageTabs().navigateToPage(Page.PII_TYPE_SETTINGS);
        if (piiManagementPage.verifyPIIExists(piiType)) {
            Logger.info("PII Type with name '" + piiType.getName() + "' already exists!");
            return;
        }
        piiManagementPage = piiManagementPage.createPII(piiType);
        Assert.assertTrue(piiManagementPage.verifyPIIExists(piiType), "PII '" + piiType.getName() + "' not found");
    }

    /**
     * Checks, if items with the name, same as piiType name, exist in list
     *
     * @param piiType {@link PIIType}
     * @return true if more than 0 items with the same name exists
     */
    public static boolean verifyPIIExists(PIIType piiType) {
        new HomePage().getUserSubMenu().navigateToPage(Page.ADMIN_PANEL_PAGE);
        UserManagementPage userManagementPage = new UserManagementPage();
        PiiManagementPage piiManagementPage = userManagementPage.getAdminPageTabs().navigateToPage(Page.PII_TYPE_SETTINGS);
        piiManagementPage.filterPii(piiType.getName());
        return piiManagementPage.verifyPIIExists(piiType);
    }

    /**
     * Edit custom Pii Type
     *
     * @param beforeEdit {@link PIIType}
     * @param afterEdit  {@link PIIType}
     */
    public static void editPiiCustomType(PIIType beforeEdit, PIIType afterEdit) {
        new HomePage().getUserSubMenu().navigateToPage(Page.ADMIN_PANEL_PAGE);
        Waiters.waitForAnimationCompleteOnPage(30);
        UserManagementPage userManagementPage = new UserManagementPage();
        PiiManagementPage piiManagementPage = userManagementPage.getAdminPageTabs().navigateToPage(Page.PII_TYPE_SETTINGS);
        if (!piiManagementPage.verifyPIIExists(beforeEdit)) {
            Logger.info("PII Type with name '" + beforeEdit.getName() + "' doesnt exist!");
            return;
        }
        piiManagementPage = piiManagementPage.editPII(beforeEdit.getName(), afterEdit);
        Assert.assertTrue(piiManagementPage.verifyPIIExists(afterEdit));
    }

    /**
     * Filters Pii Type Settings by keyword
     *
     * @param keyword keyword to be filtered
     * @return int number of filtered items
     */
    public static int filterPIIType(String keyword) {
        new HomePage().getUserSubMenu().navigateToPage(Page.ADMIN_PANEL_PAGE);
        Waiters.waitForAnimationCompleteOnPage(30);
        UserManagementPage userManagementPage = new UserManagementPage();
        PiiManagementPage piiManagementPage = userManagementPage.getAdminPageTabs().navigateToPage(Page.PII_TYPE_SETTINGS);
        int result = piiManagementPage.filterPii(keyword);
        return result;
    }
}
