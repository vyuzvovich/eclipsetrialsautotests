package businessutils;

import pages.ClassificationPage;

/**
 * Wrapping util for actions from Model Classification page
 */
public class ModelClassificationUtil {

    /**
     * Methods fills Classification table with default values from DefaultClassification.json
     */
    public static void fillClassificationTableWithDefaults() {
        ClassificationPage classificationPage = new ClassificationPage().addDefaultClassifications();
    }


}
