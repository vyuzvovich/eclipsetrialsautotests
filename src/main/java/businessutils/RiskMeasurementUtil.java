package businessutils;

import java.util.List;

/**
 * Wrapping actiona for actions with Risk management
 */
public class RiskMeasurementUtil {

    /**
     * Gets Strict L1 risk from Log Modal window
     * @param projectName name of project
     * @return Double parsed value of Strict L1 risk
     */
    public static Double getStrictRisk(String projectName){
        List<String> logsRows = HomeProjectManagementUtil.getLogsFromModalWindow(projectName);
        return HomeProjectManagementUtil.getStrictRiskFromModalWindow(logsRows);
    }

    /**
     * Gets Public risk from Log Modal window
     * @param projectName name of project
     * @return Double parsed value of Public risk
     */
    public static Double getPublicRisk(String projectName){
        List<String> logsRows = HomeProjectManagementUtil.getLogsFromModalWindow(projectName);
        return HomeProjectManagementUtil.getPublicRiskFromModalWindow(logsRows);
    }

    /**
     * Gets Acquaintance risk from Log Modal window
     * @param projectName name of project
     * @return Double parsed value of Acquaintance risk
     */
    public static Double getAcquaintanceRisk(String projectName){
        List<String> logsRows = HomeProjectManagementUtil.getLogsFromModalWindow(projectName);
        return HomeProjectManagementUtil.getAcquaintanceRiskFromModalWindow(logsRows);
    }

    /**
     * Gets Traditional risk from Log Modal window
     * @param projectName name of project
     * @return Double parsed value of Traditional risk
     */
    public static Double getTraditionalRisk(String projectName){
        List<String> logsRows = HomeProjectManagementUtil.getLogsFromModalWindow(projectName);
        return HomeProjectManagementUtil.getTraditionalRiskFromModalWindow(logsRows);
    }
}
