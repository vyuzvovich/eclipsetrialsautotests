package businessutils;

import businessobjects.DataSource;
import org.testng.Assert;
import pages.AddToClassificationPage;
import pages.ClassificationPage;

/**
 * Wrapping class for actions from Model - Data Model page
 */
public class ModelDataModelUtil {

    public static void setTablesRelationship(DataSource dataSource){
        ClassificationPage classificationPage;
        if(dataSource.getDataModels().size() == 0) {
            classificationPage = new AddToClassificationPage().navigateToClassificationPage();
            return;
        }
        classificationPage = new AddToClassificationPage().navigateToDataModel().setTableRelationShips(dataSource)
                .navigateToClassificationPage();
        Assert.assertTrue(classificationPage.verifyClassificationLoaded(dataSource));
    }
}
