package businessutils;

import businessobjects.enums.Users;
import core.logger.Logger;
import org.testng.Assert;
import pages.GroupManagementPage;
import pages.HomePage;
import pages.LoginPage;
import pages.Page;
import pages.UserManagementPage;
import waiters.Waiters;

/**
 * Wrapping util for actions with users and groups management
 */
public class UserCreationUtil {


    /**
     * This method first checks, if user exists
     * If not exists - creates a new user and assigns it to user groups
     * @param user - from {@link businessobjects.enums.Users}
     */
    public static void createUserAndAssignGroup(Users user){
        LoginPage page = new LoginPage().openPage();
        page.loginWithDefaultUser();
        new HomePage().getUserSubMenu().navigateToPage(Page.ADMIN_PANEL_PAGE);
        UserManagementPage userManagementPage = new UserManagementPage();
        Waiters.waitForURLContains(Page.USER_MANAGEMENT_PAGE.getLink(), 30);
        Waiters.waitForSpinnerDisable();

        if(!userManagementPage.verifyUserExists(user)){
            userManagementPage.createUser(user);
            Logger.info("User '" + user.getName() + "' is created successfully!");
        }
        GroupManagementPage groupManagementPage = userManagementPage.getAdminPageTabs().navigateToPage(Page.GROUP_MANAGEMENT_PAGE);
        Waiters.waitForURLContains(Page.GROUP_MANAGEMENT_PAGE.getLink(), 30);
        Waiters.waitForSpinnerDisable();
        Assert.assertTrue(groupManagementPage.addUserToGroups(user));
        Logger.info("User '" + user.getName() + "' was added to groups");
    }

}
