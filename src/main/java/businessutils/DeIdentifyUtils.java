package businessutils;

import pages.DeIdentityPage;
import pages.DirectIdentifiersPage;
import pages.HomePage;
import pages.Page;
import pages.QuasiIdentifiersPage;
import waiters.Waiters;

/**
 * Wrapper util for de-identification methods and actions
 */
public class DeIdentifyUtils {

    /**
     * Method Clicks Load Defaults Button and navigates to Quasi-Identifiers Page
     * @return {@link QuasiIdentifiersPage}
     */
    public static QuasiIdentifiersPage loadDefaultsForDirectIdentifiersPage(){
        DirectIdentifiersPage directIdentifiersPage = new DirectIdentifiersPage();
        Waiters.waitForURLContains(Page.DIRECT_IDENTIFIERS_PAGE.getLink(), 30);
        directIdentifiersPage.clickLoadDefaults();
        Waiters.waitForAnimationCompleteOnPage(30);
        return directIdentifiersPage.navigateToQuasiIdentifiers();
    }

    /**
     * Method Clicks Load Defaults on QuasiIdentifiers and navigates to De-Identity Page
     * @return {@link DeIdentityPage}
     */
    public static DeIdentityPage loadDefaultsForQuasiIdentifiersPage(){
        QuasiIdentifiersPage quasiIdentifiersPage = new QuasiIdentifiersPage();
        Waiters.waitForURLContains(Page.QUASI_IDENTIFIERS_PAGE.getLink(), 30);
        quasiIdentifiersPage.loadDefaults();
        Waiters.waitForSpinnerDisable();
        Waiters.waitForAnimationCompleteOnPage(30);
        DeIdentityPage deIdentityPage = quasiIdentifiersPage.navigateToDestinationPage();
        return deIdentityPage;
    }

    /**
     * Method fills the Destination page with settings and starts De-Identification
     * @param projectName should be name of project
     * @param exportSetting should be DeIdentityPage.#ALWAYS, DeIdentityPage.LOWRISK or DeIdentityPage.NEVER
     * @return {@link HomePage}
     */
    public static HomePage runDeIdentification(String projectName, String exportSetting){
        DeIdentityPage deIdentityPage = new DeIdentityPage();
        Waiters.waitForURLContains(Page.DE_IDENTITY_PAGE.getLink(), 30);
        HomePage homePage =  deIdentityPage.deIdentifyWithExportSettings(exportSetting)
                .waitForDeidentificationCompleted(projectName);
        return homePage;
    }


}
