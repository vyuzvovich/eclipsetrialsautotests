package businessutils;

import businessobjects.ClassificationTable;
import businessobjects.DataSource;
import core.logger.Logger;
import org.testng.Assert;
import pages.AddToClassificationPage;
import pages.LocatePage;
import pages.ReconcileProjectsPage;
import pages.UploadCSVPage;
import waiters.Waiters;

import java.util.Map;

/**
 * Wrapping util for actions from Locate Data Sources
 */
public class LocateDataSourcesUtil {

    /**
     * Method creates new CSV DataSource
     * @param dataSource {@link DataSource}
     */
    public static void createNewCSVDataSource(DataSource dataSource) {
        LocatePage locatePage = new LocatePage();
        Waiters.waitForAnimationCompleteOnPage(60);
        locatePage.checkPageIsLoaded();
        locatePage.createDataSource(dataSource);
        UploadCSVPage uploadPage = new UploadCSVPage();
        uploadPage.upload(dataSource);
        if(!uploadPage.verifyDataSourceUploaded(dataSource))
            Logger.error("Data Source is not loaded correctly");
    }

    /**
     * Method adds all collumns to Classification
     * @param dataSource
     */
    public static void addAllColumnsToClassification(DataSource dataSource) {
        AddToClassificationPage addToClassificationPage = new UploadCSVPage().navigateToAddClassification();
        Assert.assertTrue(addToClassificationPage.verifyTablesAvailable(dataSource));
        Assert.assertTrue(addToClassificationPage.selectAllColumns(dataSource));
    }

    /**
     * Method selects template and matches template tables with project tables
     * @param templateName
     * @param matchingMap
     */
    public static void applyTemplateAndMatchTables(String templateName, Map<ClassificationTable, ClassificationTable> matchingMap) {
        AddToClassificationPage addToClassificationPage = new AddToClassificationPage();
        ReconcileProjectsPage reconcileProjectsPage = addToClassificationPage.clickApplyATemplateBtn();
        reconcileProjectsPage.matchChosenTables(templateName, matchingMap);
        reconcileProjectsPage.navigateToClassificationPage();
    }

}
