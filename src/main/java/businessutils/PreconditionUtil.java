package businessutils;

import businessobjects.DataSource;
import businessobjects.Project;
import pages.ClassificationPage;
import pages.ContextPage;
import pages.HomePage;

/**
 * Wrapping util for complicated precondition actions
 */
public class PreconditionUtil {

    /**
     * Method creates new project and follow the happy path up to the end of Risk management
     * Measure Risk is performed with defaults
     * @param project
     * @param dataSource
     */
    public static void createProjectUpToRiskManagement(Project project, DataSource dataSource){
        HomeProjectManagementUtil.createNewProjectOnHomePage(project);

        HomeProjectManagementUtil.clickOnProject(project);

        LocateDataSourcesUtil.createNewCSVDataSource(dataSource);

        LocateDataSourcesUtil.addAllColumnsToClassification(dataSource);

        ModelDataModelUtil.setTablesRelationship(dataSource);

        ModelClassificationUtil.fillClassificationTableWithDefaults();
        ContextPage contextPage = new ClassificationPage().navigateToDataGroups().navigateToContextPage();
        contextPage.measureRisk();
        new HomePage().waitForRiskMeasurementCompleted(project.getName());
    }

    /**
     * Method creates new project and follow the happy path up to the end of Risk management
     * Measure Risk is performed with specific Settings
     * @param project
     * @param dataSource
     */
    public static void createProjectUpToRiskManagementWithCustomRMSettings(Project project, DataSource dataSource, int value){
        HomeProjectManagementUtil.createNewProjectOnHomePage(project);

        HomeProjectManagementUtil.clickOnProject(project);

        LocateDataSourcesUtil.createNewCSVDataSource(dataSource);

        LocateDataSourcesUtil.addAllColumnsToClassification(dataSource);

        ModelDataModelUtil.setTablesRelationship(dataSource);

        ModelClassificationUtil.fillClassificationTableWithDefaults();
        ContextPage contextPage = new ClassificationPage().navigateToDataGroups().navigateToContextPage();
        contextPage.setCustomPopulation(value);
        contextPage.measureRisk();
        new HomePage().waitForRiskMeasurementCompleted(project.getName());
    }
}
